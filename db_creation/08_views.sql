USE [ifw9mb-BCwaterfowlSurvey]
GO

/*
	For use by anyone, however, their primary purpose is for rendering the buffers in QGIS.
	For some reason QGIS recognizes that the ref_transect_buffered has 2 geography columns, 
	but when rendered in QGIS 2.18, it always renders the 5000m buffer column.  

	By creating a view for each buffer distance seems to resolve this issue and render each 
	buffer distance correctly.
*/
CREATE VIEW transect_buffer_200m
AS
	SELECT 
		  transect_buffered AS [object_fid]
		, master_line
		, ecoregion_line
		, segment
		, ecoregion_id
		, area_sqkm
		, geog_200m_epsg4326
	FROM ref_transect_buffered
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A view which holds only the details and geometries for the 200m buffer.  Aid in rendering in GIS applications that have difficulty with SQL tables with multiple geometries.', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'transect_buffer_200m'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description',  @value=N'A unique identifier of a 200m buffer segment', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'object_fid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sequential line number begininning at the north of the survey area increasing south.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'master_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The transect line number within the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'ecoregion_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A continuous portion of a transect line', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'segment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The area of the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'area_sqkm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The spatial object which holds the 200m buffer polygon', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_200m', @level2type=N'COLUMN', @level2name=N'geog_200m_epsg4326'
GO


CREATE VIEW transect_buffer_5000m
AS
	SELECT 
		  transect_buffered AS [object_fid]
		, master_line
		, ecoregion_line
		, segment
		, ecoregion_id
		, geog_5000m_epsg4326
	FROM ref_transect_buffered
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A view which holds only the details and geometries for the 5000m buffer.  Aid in rendering in GIS applications that have difficulty with SQL tables with multiple geometries.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'transect_buffer_5000m'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A unique identifier of a 5000m buffer segment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'object_fid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sequential line number begininning at the north of the survey area increasing south.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'master_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The transect line number within the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'ecoregion_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A continuous portion of a transect line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'segment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The spatial object which holds the 5000m buffer polygon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'transect_buffer_5000m', @level2type=N'COLUMN', @level2name=N'geog_5000m_epsg4326'
GO


INSERT INTO geometry_columns
VALUES 
('ifw9mb-BCWaterfowlSurvey','dbo','transect_buffer_200m','geog_200m_epsg4326',2,4326,'POLYGON'),
('ifw9mb-BCWaterfowlSurvey','dbo','transect_buffer_5000m','geog_5000m_epsg4326',2,4326,'POLYGON')
GO

/*
	Create design matrix for BC all years of the survey.  
*/
CREATE VIEW SurveyEffort_by_year
AS 
SELECT ROW_NUMBER() OVER (ORDER BY [Year], ecoregion_id, transect_number, segment) AS fid
	, Year
	, transect_id
	, transect_number
	, segment
	, ecoregion_id
	, ecoregion
	, area_sqkm
	, geog_200m_epsg4326
FROM (
SELECT yr.[Year]
	, e.ecoregion AS ecoregion_id
	, t.transect_buffered AS transect_id
	, t.ecoregion_line AS transect_number
	, t.segment
	, e.[description] AS ecoregion
	, t.area_sqkm
	, t.geog_200m_epsg4326
FROM ref_transect_buffered t
CROSS JOIN (SELECT DISTINCT YEAR(observation_datetime_utc) AS [Year] FROM Observation) yr
LEFT JOIN ref_ecoregion e
	ON t.ecoregion_id = e.ecoregion
FULL OUTER JOIN transect_notflown mis
	ON yr.[year] = mis.[year] AND t.transect_buffered = mis.transect_id
WHERE mis.transect_id IS NULL) AS Z;
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This view creates a design matrix for survey for all years of the survey. The view also returns the 200m survey width', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An incremental number to identify the row, which is represents the unique combination of year, species, ecoregion, transect and segment.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'fid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Survey Year', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'transect key value', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'transect_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of the transect within the ecoregion surveyed', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'transect_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The continuous portion of the transect surveyed.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'segment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id number of the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Area of the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'area_sqkm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Spatial object of the 200m buffered transect' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'SurveyEffort_by_year', @level2type=N'COLUMN', @level2name=N'geog_200m_epsg4326'
GO

INSERT INTO geometry_columns
VALUES
('ifw9mb-BCWaterfowlSurvey','dbo','SurveyEffort_by_year','geog_200m_epsg4326', 2,4326,'POLYGON')
GO

/*
	Create a view which allows for GIS display of records with errors
*/
CREATE VIEW [dbo].[Records_with_errors]
AS 
	SELECT
		  [observation_sid]
		, [cws_record_identifier]
		, [cws_point_identifier]
		, YEAR([observation_datetime_utc]) AS [Year]
		, [cws_ecoregion_assignment] AS [cws_assigned_ecoregion]
		, e.[description] as [ecoregion_name]
		, [cws_transect_assignment] AS [cws_assigned_transect]
		, oe.[measured_ecoregion_id] AS [computed_ecoregion]
		, oe.[measured_ecoregion_line] AS [computed_transect]
		, oe.error_description
		, o.location_epsg4326
	FROM observation o
	RIGHT JOIN observation_transect_error oe
		ON o.Observation_sid = oe.observation_id
	LEFT JOIN ref_ecoregion e
	ON o.cws_ecoregion_assignment = e.ecoregion
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A view to display observation with errors; those where our ecoregion and transect, differ from what CWS assigned them.', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Records_with_errors'
GO

EXEC sys.sp_addextendedproperty @name=N'Observation Identifier from observation table' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'observation_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'CWS value for cross reference in their system' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'cws_record_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'CWS value for cross reference in their system' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'cws_point_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'Survey year', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'The ecoregion the observation was assigned to by CWS' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'cws_assigned_ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'The transect the observation was assigned to by CWS' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'cws_assigned_transect'
GO
EXEC sys.sp_addextendedproperty @name=N'This represent the ecoregion id that was obtained using a spatial intersect method from data contained in the database.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'computed_ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'This represent the transect that was obtained using a spatial intersect method from data contained in the database.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'computed_transect'
GO
EXEC sys.sp_addextendedproperty @name=N'The error text returned by the TRG_INSERT_TransObsAssoc trigger.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'error_description'
GO
EXEC sys.sp_addextendedproperty @name=N'The spatial location of the observation' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'Records_with_errors', @level2type=N'COLUMN', @level2name=N'location_epsg4326'
GO

INSERT INTO geometry_columns
VALUES
('ifw9mb-BCWaterfowlSurvey','dbo','records_with_errors','location_epsg4326', 2,4326,'POINT')
GO

/*
	Create a generalized flat view of all observations, that people could draw from 
	for analysis.  However, users would be required to re-implement logic of social
	groups to calculate tips and tibs.
*/

CREATE VIEW observations_flat_view
AS
SELECT 
	  o.[observation_sid]
	, o.[observation_datetime_utc]
	, t.ecoregion_id
	, e.[description] as ecoregion
	, t.[ecoregion_line] as transect
	, o.[species_id]
	, s.[common_name]
	, o.[social_group_id]
	, sg.[description] AS social_group_description	
	, CASE WHEN o.social_group_id BETWEEN 1 AND 5 THEN males
		WHEN social_group_id IN (6,9) THEN females
		WHEN social_group_id = 7 THEN males + females
		WHEN social_group_id = 8 THEN unknown END AS [count]
	, o.location_epsg4326.Lat AS latitude
	, o.location_epsg4326.Long AS longitude
	, o.location_epsg4326
FROM Observation O
LEFT JOIN ref_species s
	ON O.species_id = s.species
LEFT JOIN ref_social_group sg
	ON o.social_group_id = sg.social_group
LEFT JOIN Observation_transect ot
	ON o.observation_sid = ot.observation_id
LEFT JOIN ref_transect t
	ON ot.transect_id = t.transect
LEFT JOIN ref_ecoregion e
	on t.ecoregion_id = e.ecoregion;
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A simplified view of observations. The view reverts the male/female/unknown columns back to a count and social group.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Database id number of the observation.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'observation_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time the observation occurred.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'observation_datetime_utc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ecoregion number', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Human readable description of the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The transect number within the ecoregion', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'transect'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The 4-letter character code of the sepcies', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'species_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Species common name', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'common_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Social group identifier', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'social_group_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Human readable description of the social group', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'social_group_description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count or value of the observation', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Observation latitude coordinate', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Observation longitude coordinate', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Spatial point location of where the observation occurred.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'observations_flat_view', @level2type=N'COLUMN', @level2name=N'location_epsg4326'
GO


INSERT INTO geometry_columns
VALUES
('ifw9mb-BCWaterfowlSurvey','dbo','observations_flat_view','location_epsg4326', 2,4326,'POINT')
GO

/*
	Generate a summary table of TIPs, TIBs by year, species, transect

	-- do I need 0's for transect surveyed but no observations occurred.
*/
CREATE VIEW tib_tip_by_year_species_transect
AS
SELECT YEAR(obs.observation_datetime_utc) AS Year
	, spp.fws_alpha_grouped AS Species
	, t.ecoregion_id
	, er.[description] AS ecoregion
	, t.ecoregion_line as transect
	, SUM(dbo.calculateTIPs(obs.species_id, social_group_id, males, females)) AS TIPs
	, SUM(dbo.calculateTIBs(obs.species_id, social_group_id, males, females, unknown)) AS TIBs
	, er.area_hectares AS ecoregion_area_ha
	, SUM(t.area_sqkm * 100) AS transect_area_surveyed_ha
FROM observation obs
LEFT JOIN observation_transect obs_tran
	ON obs.observation_sid = obs_tran.observation_id
LEFT JOIN ref_transect_buffered t
	ON obs_tran.transect_id = t.transect_buffered
LEFT JOIN ref_species spp
	ON obs.species_id = spp.species
LEFT JOIN ref_ecoregion er
	ON t.ecoregion_id = er.ecoregion
GROUP BY YEAR(obs.observation_datetime_utc), spp.fws_alpha_grouped, er.[description], t.ecoregion_line, t.ecoregion_id, er.area_hectares
UNION ALL
SELECT YEAR(obs.observation_datetime_utc) AS Year
	, 'TODU' AS Species
	, t.ecoregion_id
	, er.[description] AS ecoregion
	, t.ecoregion_line as transect
	, SUM(dbo.calculateTIPs(species_id, social_group_id, males, females)) AS TIPs
	, SUM(dbo.calculateTIBs(species_id, social_group_id, males, females, unknown)) AS TIBs
	, er.area_hectares AS ecoregionArea_ha
	, SUM(t.area_sqkm * 100) AS transect_area_surveyed_ha
FROM observation obs
LEFT JOIN observation_transect obs_tran
	ON obs.observation_sid = obs_tran.observation_id
LEFT JOIN ref_transect_buffered t
	ON obs_tran.transect_id = t.transect_buffered
LEFT JOIN ref_ecoregion er
	ON t.ecoregion_id = er.ecoregion
WHERE obs.species_id IN (SELECT DISTINCT species_id FROM ref_species WHERE is_included_todu = 1)
GROUP BY YEAR(obs.observation_datetime_utc), er.[description], t.ecoregion_line, t.ecoregion_id, er.area_hectares
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Generate a summary table of TIPs, TIBs by year, species, transect.  It also provides a line for total TIBs, TIPs with a species code of TODU.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Survey Year', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'4 letter species code', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'Species'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ecoregion number', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ecoregion name', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transect number within the ecoregion.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'transect'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The calculated number of Total Indicated Pairs', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'TIPs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The calculated number of Total Indicated Birds', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'TIBs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The area of the ecoregion in hectares', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'ecoregion_area_ha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The area of the survey strip width in hectares', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tib_tip_by_year_species_transect', @level2type=N'COLUMN', @level2name=N'transect_area_surveyed_ha'
GO

-- Generate a summary table of TIPs, TIBs by year, species, transect
-- The view includes zeros for all species (except TODU) that could have been observed but were not
-- on all transects flown.
-- 
-- The view does not include in counts observations that are associated with transects
-- that were noted to be not flown.  Observations on transects not flown result from the 
-- post hoc assocation of the point and transect (spatial join).

CREATE VIEW tip_and_tib_counts
AS
(SELECT x.[year]
	, x.[species]
	, x.[ecoregion_id]
	, x.[ecoregion]
	, x.[transect]
	, x.[ecoregion_area_ha]
	, x.[transect_area_surveyed_ha]
	, COALESCE(dat.TIBs, 0) AS TIBs
	, COALESCE(dat.TIPs, 0) AS TIPs
FROM
(SELECT
	  dm.[year]
	, s.fws_alpha_grouped AS species
	, dm.[ecoregion_id]
	, dm.[ecoregion]
	, dm.transect_number AS transect	
	, e.area_hectares AS ecoregion_area_ha
	, SUM(dm.area_sqkm) * 100  AS transect_area_surveyed_ha
FROM surveyeffort_by_year dm
LEFT JOIN ref_ecoregion e
	ON e.ecoregion = dm.ecoregion_id
CROSS JOIN (SELECT DISTINCT fws_alpha_grouped FROM ref_species) s
GROUP BY dm.[Year], s.fws_alpha_grouped, dm.ecoregion_id, dm.ecoregion, e.area_hectares, dm.transect_number) x
LEFT JOIN (SELECT YEAR(obs.observation_datetime_utc) AS Year
				, spp.fws_alpha_grouped AS Species
				, t.ecoregion_id
				, t.ecoregion_line AS transect
				, SUM(dbo.calculateTIPs(obs.species_id, social_group_id, males, females)) AS TIPs
				, SUM(dbo.calculateTIBs(obs.species_id, social_group_id, males, females, unknown)) AS TIBs
			FROM observation obs
			LEFT JOIN observation_transect obs_tran
				ON obs.Observation_sid = obs_tran.observation_id
			LEFT JOIN ref_transect t
				ON obs_tran.transect_id = t.transect
			LEFT JOIN ref_species spp
				ON obs.species_id = spp.species
			LEFT JOIN ref_ecoregion er
				ON t.ecoregion_id = er.ecoregion
			LEFT join transect_notflown tnf
				ON obs_tran.transect_id = tnf.transect_id AND
				YEAR(obs.observation_datetime_utc) = tnf.[year]
			WHERE transect_notflown_sid IS NULL -- check this.
			GROUP BY YEAR(obs.observation_datetime_utc), spp.fws_alpha_grouped, t.ecoregion_line, t.ecoregion_id, er.area_sqkm
			) as dat
ON x.[year] = dat.[year] and x.species = dat.Species and x.ecoregion_id = dat.ecoregion_id and x.transect = dat.transect
WHERE x.species <> 'TODU')
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This view summarizes all of the observatoin data and produces a table of table of total indicated pairs (TIPs), and total indicated birds (TIBs) by year, species, transect. For each record, the table also reports the total area of the ecoregion and the total area of the transect surveyed (200m on either side of the transect line).  The view includes zeros for all species that could have been observed but were not, for all transects flown. The view does not include in counts observations that are associated with transects that were noted to be not flown.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'tip_and_tib_counts'
GO

EXEC sys.sp_addextendedproperty @name=N'Survey year', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'year'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'4-letter species code', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'species'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'The id number of the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'The name of the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'ecoregion'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'Transect number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'transect'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'Total area of the ecoregion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'ecoregion_area_ha'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'Total area of the 200m transect survey width. The area value does account for the irregular transect ends where it meets adjoinging ecoregions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'transect_area_surveyed_ha'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'The number of total indicated birds' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'TIBs'
GO
EXEC sys.sp_addextendedproperty @name='MS_Description', @value=N'The number of total indicated pairs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW', @level1name=N'tip_and_tib_counts', @level2type=N'COLUMN', @level2name=N'TIPs'
GO

/*
	Create view which aggregates across the strata for each species and year. Since 
	there are no strata for TODU, tack them on at the end.  Report CV along with 
	estimate and standard error since, this is what was historically provided in 
	the SAS report.
*/
CREATE VIEW estimates_species_totals
AS 
	SELECT Year
		, species_id
		, CAST(SUM(estimate) AS DECIMAL(12,4)) AS estimate
		, CAST(SQRT(SUM(variance)) AS DECIMAL(12,4)) AS standard_error
		, CASE WHEN SUM(variance) = 0 THEN 0 
			ELSE CAST(SUM(estimate) / SQRT(SUM(variance)) AS DECIMAL(8,4)) END AS CV_estimate  
	FROM Estimate
	WHERE species_id <> 'TODU'
	GROUP BY Year, species_id
	UNION
	SELECT Year
		, species_id
		, CAST(estimate AS DECIMAL(12,4)) AS estimate
		, CAST(SQRT(variance) AS DECIMAL(12,4)) AS standard_error
		, CAST(estimate / SQRT(variance) AS DECIMAL(8,4)) AS CV_esimate
	FROM Estimate
	WHERE species_id = 'TODU'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This view creates estimates for each species and year for the entire survey area. Also reports estimates for total ducks.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Survey year', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals', @level2type=N'COLUMN', @level2name=N'year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'species identifier', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals', @level2type=N'COLUMN', @level2name=N'species_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of the estimate', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals', @level2type=N'COLUMN', @level2name=N'estimate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The standard error of the estimate', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals', @level2type=N'COLUMN', @level2name=N'standard_error'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The coefficient of variation around the estimate.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'estimates_species_totals', @level2type=N'COLUMN', @level2name=N'cv_estimate'
GO
