USE [ifw9mb-BCwaterfowlSurvey]
GO

CREATE TABLE [ref_species] (
   [species] CHAR(4) NOT NULL
  ,[aou_alpha] CHAR(4) NOT NULL
  ,[aou_number] INTEGER NOT NULL
  ,[fws_alpha_grouped] CHAR(4) NOT NULL
  ,[common_name] VARCHAR(200) NOT NULL
  ,[is_waterfowl] BIT NOT NULL
  ,[is_monomorphic] BIT NOT NULL
  ,[is_included_todu] BIT NOT NULL DEFAULT 0
  ,[itis_code] INTEGER NULL
  , PRIMARY KEY CLUSTERED (species)
);
GO

-- Create indexes on aou_alpha, aou_number, fws_alpha_grouped, itis_code
CREATE INDEX idx_species_aouAlpha ON ref_species (aou_alpha);
CREATE INDEX idx_species_aouNum ON ref_species (aou_number);
CREATE INDEX idx_species_fws_alpha ON ref_species (fws_alpha_grouped);
CREATE INDEX idx_species_ItisCode ON ref_species (itis_code);


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is primarily a lookup table which maps codes used by CWS to identify species, to a common set of codes used by the USFWS. In addition, the table can provide additional information like common name or ITIS identifier for improved accessibility when distributing data.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ref_species'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'species codes provided by CWS in raw data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'species'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The 4-letter alpha designation used by USFWS for species identification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'aou_alpha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'a 2-4 digit number created by the AOU (no longer used), but still used the the USGS Bird Banding Lab to identify species. When the record refers to a species group a custom aou number is created, typically the first 2 digits representing the genus plus 99 (e.g., lesser scaup = 1480, greater scaup = 1490, scaup = 1499).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'aou_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To be consistent with other FWS surveys, some species are difficult to identify from the air, and are grouped for analysis.  Other species may be grouped to permit estimation, since very few observations may occur during a survey. Species typically grouped area goldeneyes, scaup, scoters, loons, swans, pelicans, grebes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'fws_alpha_grouped'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Species common name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'common_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A Boolean (bit) column to identify whether the species is a "waterfowl species" as opposed to a non-waterfowl species.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'is_waterfowl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An identifier for species which are sexually monomorphic, or that males and females cannot be identified from plumage characteristics.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'is_monomorphic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An identifier which identifies whether a species is included in the definition of total ducks.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'is_included_todu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' ITIS taxonomic identifier.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_species', @level2type=N'COLUMN', @level2name=N'itis_code'
GO


CREATE TABLE [ref_social_group] (
	  [social_group] INTEGER IDENTITY(1,1) NOT NULL
	, [description] VARCHAR(100) NULL
	, [cws_column_name] VARCHAR(50) NULL
	PRIMARY KEY (social_group)
);
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currently any bird, or groups of birds observed during a survey are classified into 1 of 8 possible social groupings. These groupings are pairs, lone males, lone females, a group of 2 males, a group or 3 males, a group of 4 males, a mixed sex group, and unknown group.  The unknown group is reserved for monomorphic species.  It was a design choice to make this a foreign key lookup table to ensure that records with invalid groupings cannot be added, and to allow for future flexibility if new social groups are defined.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_social_group'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier for defining social groups' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_social_group', @level2type=N'COLUMN',@level2name=N'social_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A text description of the social group.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_social_group', @level2type=N'COLUMN', @level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This corresponds to the name of the column associated with the social grouping in the datafile provided to us by CWS.  NOTE. Their file is in the wide format.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_social_group', @level2type=N'COLUMN', @level2name=N'cws_column_name'
GO