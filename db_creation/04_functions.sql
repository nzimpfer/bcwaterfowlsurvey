USE [ifw9mb-BCwaterfowlSurvey]
GO
/*
	This function checks if a species is monomorphic. Returns 1 for TRUE and 0 for FALSE. 
	This is a necessary check function as acceptable social group codes for monomorphic 
	species are pairs, and uknowns.
*/
CREATE FUNCTION dbo.chk_IsMonomorphic ( @species_id VARCHAR)
RETURNS BIT
AS 
BEGIN
	DECLARE @retval BIT
	SELECT @retval = is_monomorphic FROM ref_species WHERE species = @species_id
	RETURN @retval
END
GO


/* 
	Given a long table format for observations calculate TIP/TIBs 

	should handle situation when the species column passed is a real species or species 
	groups like SCAU, GOLD, etc.  
*/

/* TIP calculations  */
CREATE FUNCTION dbo.calculateTIPs(@species CHAR(4), @social_group INT, @males INT, @females INT)
RETURNS INT
AS
BEGIN
	DECLARE @TIP_Value INT

	SELECT @TIP_Value = CASE 
	-- Social Group = Pair
	WHEN @social_group = 1 THEN
		CASE
			WHEN @species IN ('AMCO','SWAN','LOON','PELI','TRUS','SNOG','GREB') THEN 0 
			ELSE @males
		END
	-- Social Group = Lone Male
	WHEN @social_group = 5 THEN
		CASE
			WHEN @species IN ('REDH','RUDU','RNDU','SCAU') THEN 0
			ELSE @males 
		END
	-- Social Group = Lone Female
	WHEN @social_group = 6 THEN
		CASE
			WHEN @species IN ('REDH','RUDU','RNDU','SCAU') THEN @females
			ELSE 0
		END
	-- Social Group = Grouped Males 
	WHEN @social_group IN (2,3,4) THEN
		CASE 
			WHEN @species IN ('REDH','RUDU','RNDU','SCAU') THEN 0 
			ELSE @males 
		END
		
	-- else capture unknown sex records and mixed sex groups.
	ELSE 0 
	END

RETURN @TIP_Value
END
GO


/* TIB calculations 
redhead, lesser scaup, ring-necked duck, and ruddy duck - 
Social groups included are:  lone females, lone males, pairs, and groups. All others are not included (0)

American coot, geese, swans, loons, pelicans, and grebes (lone male = lone bird for monomorphic species) - 
Social groups included are: lone males, pairs, and groups All others are not included (0)

all other species - Social groups included are: 2 x lone males, pairs, groups
*/
CREATE FUNCTION dbo.calculateTIBs(@species CHAR(4), @social_group INT, @males INT, @females INT, @unknown INT)
RETURNS INT
AS
BEGIN
	DECLARE @TIB_Value INT

	SELECT @TIB_Value = CASE 
		-- Social Group = Pair
		WHEN @social_group = 1 THEN @males + @females
		
		-- Social Group = Lone Male
		WHEN @social_group = 5 THEN 
			CASE
				WHEN @species IN ('REDH','RUDU','RNDU','SCAU','AMCO',
								  'SWAN','LOON','PELI','TRUS','SNOG',
								  'GREB') THEN @males
				ELSE @males * 2
			END

		-- Social Group = Lone Female
		WHEN @social_group = 6 THEN 
			CASE
				WHEN @species IN ('REDH','RUDU','RNDU','SCAU','AMCO',
								  'SWAN','LOON','PELI','TRUS','SNOG',
								  'GREB') THEN @females
				ELSE 0
			END

		-- Social Group = Grouped Males 
		WHEN @social_group IN (2,3,4) THEN 
			CASE
				WHEN @species IN ('REDH','RUDU','RNDU','SCAU','AMCO',
								  'SWAN','LOON','PELI','TRUS','SNOG',
								  'GREB') THEN @males
				ELSE @males * 2
			END

		-- Social Group = Mixed sex groups
		WHEN @social_group = 7 THEN @males + @females

		WHEN @social_group = 8 THEN 
			CASE 
				WHEN @species IN ('REDH','RUDU','RNDU','SCAU','CAGO','GWFG',
								  'LSGO','AMCO','SWAN','LOON','PELI','TRUS',
								  'SNOG','GREB') THEN @unknown
				ELSE 0
			END
	
		ELSE 0 END 

RETURN @TIB_Value
END
GO
