USE [ifw9mb-BCwaterfowlSurvey]
GO

-- Table to store individual observation records. 
CREATE TABLE [observation] (
    [Observation_sid] INTEGER IDENTITY(1,1)
  , [cws_record_identifier] INTEGER NOT NULL
  , [cws_point_identifier] VARCHAR(10) NOT NULL
  , [observation_datetime_utc] DATETIME NOT NULL
  , [location_epsg4326] GEOGRAPHY NOT NULL
  , [cws_ecoregion_assignment] INTEGER NOT NULL
  , [cws_transect_assignment] INTEGER NOT NULL
  , [species_id] CHAR(4) NOT NULL
  , [social_group_id] INTEGER NOT NULL
  , [males] INTEGER NULL
  , [females] INTEGER NULL
  , [unknown] INTEGER NULL
  , PRIMARY KEY (Observation_sid)
  , CONSTRAINT fk_observation_speciesid
	FOREIGN KEY (species_id)
	REFERENCES ref_species([species])
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
  , CONSTRAINT fk_observation_socialgroup
	FOREIGN KEY (social_group_id)
	REFERENCES ref_social_group(social_group)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
  -- For observations with an indeterminate (unknown) social grouping, males and females columns must be NULL.
  -- fail when males or females is not null
  , CONSTRAINT chk_unknwn_grp_has_value_mf CHECK((social_group_id = 8 AND males IS NULL AND females IS NULL) OR
    social_group_id <> 8)

  -- For observations with social groups of pair or mixed sex groups, counts must occur in males and females.
  -- fail when unknown is not null.
  , CONSTRAINT chk_pair_mixed_have_value_unk CHECK((social_group_id IN (1,7) AND unknown IS NULL) OR
    social_group_id NOT IN (1,7))

   -- For observations with social_groups of grouped males, lone males, counts in males are valid only.
   -- fail when counts occur in unknown or female
  , CONSTRAINT chk_male_grps_have_value_unk_f CHECK((social_group_id BETWEEN 2 AND 5 AND unknown IS NULL AND females IS NULL) OR
    social_group_id NOT BETWEEN 2 AND 5)
  
  -- For observations with social group of lone females or bird_on_nest, counts must occur in female column.
  -- fail on presence of values in males, or unknown columns.
  , CONSTRAINT chk_female_grp_has_value_unk_m CHECK((social_group_id IN (6,9)  AND unknown IS NULL AND males IS NULL) OR
    social_group_id <> 6)
  
  -- For observations of monomorphic species (Geese, Loons, Grebes, Pelicans, Coots, Cormorants, Cranes, Swans), 
  -- acceptable social groups are pairs and unknown only.
  , CONSTRAINT chk_monomorphic_spp_invalid_group CHECK(dbo.chk_IsMonomorphic(species_id) = 1 AND social_group_id IN (1,8,9) OR
    dbo.chk_IsMonomorphic(species_id) = 0)

  -- Only allow spatial objects which are points to be added
  , CONSTRAINT chk_observation_SpatialGeom CHECK (location_epsg4326.STGeometryType() = 'POINT')

  -- All spatial objects must be WGS84 datum
  , CONSTRAINT chk_observation_SRID CHECK (location_epsg4326.STSrid = 4326)
);
GO

CREATE INDEX idx_observation_species ON Observation (species_id);
CREATE INDEX idx_observation_socialGroup ON Observation (social_group_id);

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores individual observation records' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An incremental id number to uniquely identify each observation.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN',@level2name=N'observation_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponds to the row number of the observation in the raw data file provided by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'cws_record_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponds to an internal code used by CWS, provided in the raw datafile. If this value was absent in the raw datafile, it defaults to the same value at the value in cws_record_identifer.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'cws_point_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time in which the observation occurred in UTC.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'observation_datetime_utc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The location of the helicopter where the observation was made in WGS84.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'location_epsg4326'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the ecoregion the observation was assigned to by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'cws_ecoregion_assignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of the transect the observation was assigned to by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'cws_transect_assignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Species code identifier ' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'species_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'social group identifier' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'social_group_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of birds in the social grouping that could be identified as males' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'males'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of birds in the social grouping that could be identified as females' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'females'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of birds in the social grouping where the sex could not be determined. This primarily applies to monomorphic species where sex cannot be determined unless the bird is in hand.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'COLUMN', @level2name=N'unknown'
GO

EXEC sys.sp_addextendedproperty @name=N'DEV_Comment', @value=N'There are 3 Triggers attached to this table. Trigger TRG_INSERT_TransOsbAssoc performs a spatial intersect against the 200m and 500m buffers found in [dbo].[ref_transect_buffered] on observations that are in the process of being added to the table.  If the location of the observation falls in either buffer and it matches the ecoregion and transect called by CWS, a record is added to the observation_transect table.  If the observation doesn''t fall inside a buffer or CWS values of ecoregion and transect don''t match a record is added to observation_transect_error.  The TRG_UPDATE_TransObsAssoc handles the same logic when an observation is updated.  In addition, if the original record was an error and is now valid, the error record is removed from the observation_transect_error table. Finally, the TRG_UPDATE_DEL_Observation performs audit logging of original values to audit_observation when an observation is updated or deleted.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For observations with social group of lone females or bird_on_nest, counts must occur in female column. Fail on presence of values in males, or unknown columns.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_female_grp_has_value_unk_m'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For observations with social_groups of grouped males, lone males, counts in males are valid only. Fail when counts occur in unknown or female' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_male_grps_have_value_unk_f'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For observations of monomorphic species (Geese, Loons, Grebes, Pelicans, Coots, Cormorants, Cranes, Swans), acceptable social groups are pairs and unknown only.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_monomorphic_spp_invalid_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Only allow spatial objects which are points to be added' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_observation_SpatialGeom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'All spatial objects must be WGS84 datum.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_observation_SRID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For observations with social groups of pair or mixed sex groups, counts must occur in males and females. Fail when unknown is not null.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_pair_mixed_have_value_unk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For observations with an indeterminate (unknown) social grouping, males and females columns must be NULL. Fail when males or females is not null' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation', @level2type=N'CONSTRAINT', @level2name=N'chk_unknwn_grp_has_value_mf'
GO


CREATE TABLE observation_transect (
    [observation_id] INTEGER NOT NULL
  , [transect_id] INTEGER NOT NULL
  , [validated_buffer] INTEGER NOT NULL
  , PRIMARY KEY (observation_id)
  , CONSTRAINT fk_observation_ObservationTransect
	FOREIGN KEY (observation_id)
	REFERENCES Observation(observation_sid)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
  , CONSTRAINT fk_transect_ObservationTransect
	FOREIGN KEY (transect_id)
	REFERENCES ref_transect(transect)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
);
GO

CREATE INDEX idx_obstransect_transect ON Observation_transect (transect_id)


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table stores result of spatial join performed when records are inserted' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect'
GO
EXEC sys.sp_addextendedproperty @name=N'DBA_Note', @value=N'All insertions and updates to this table are handled by the TRG_INSERT_TranObsAssoc and TRG_UPDATE_TranObsAssoc triggers on the observation table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect'
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to observation' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect', @level2type=N'COLUMN',@level2name=N'observation_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to transect' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect', @level2type=N'COLUMN', @level2name=N'transect_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The buffer that was used to associate the observation with a transect (point in polygon).' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect', @level2type=N'COLUMN', @level2name=N'validated_buffer'
GO



CREATE TABLE observation_transect_error (
	   [observation_trasect_error_sid] INTEGER IDENTITY(1,1)
	 , [observation_id] INT NOT NULL
	 , [measured_ecoregion_id] INT NULL
	 , [measured_ecoregion_line] INT NULL
	 , [error_description] VARCHAR(MAX) NOT NULL
	 , PRIMARY KEY (observation_trasect_error_sid)
	 , CONSTRAINT fk_observation_transect_error
		FOREIGN KEY (observation_id)
		REFERENCES observation(observation_sid)
		ON DELETE CASCADE
		ON UPDATE NO ACTION
)
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When performing spatial joins on observations (point in polygon), observations that fall out of the survey area, or the ecoregion or transect assignment by CWS are different that the spatial join returns are flagged as errors and store in this table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error'
GO
EXEC sys.sp_addextendedproperty @name=N'DBA_Note', @value=N'All insertions and updates to this table are handled by the TRG_INSERT_TranObsAssoc and TRG_UPDATE_TranObsAssoc triggers on the observation table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier of error records.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error', @level2type=N'COLUMN',@level2name=N'observation_trasect_error_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to observation' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error', @level2type=N'COLUMN', @level2name=N'observation_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ecoregion assigned through the spatial intersect within the TransObsAssoc Trigger' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error', @level2type=N'COLUMN', @level2name=N'measured_ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ecoregion transect assigned through the spatial intersect within the TransObsAssoc Trigger' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error', @level2type=N'COLUMN', @level2name=N'measured_ecoregion_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A description of the type of error.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'observation_transect_error', @level2type=N'COLUMN', @level2name=N'error_description'
GO



CREATE TABLE transect_notflown (
	  [transect_notflown_sid] INTEGER IDENTITY(1,1) NOT NULL
	, [transect_id] INTEGER NOT NULL
	, [year] INTEGER NOT NULL CHECK([year] <= YEAR(SYSUTCDATETIME()) )
	, PRIMARY KEY (transect_notflown_sid)
	, CONSTRAINT fk_transect_missedTransect
		FOREIGN KEY (transect_id)
		REFERENCES ref_transect(transect)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
	, CONSTRAINT UC_transect_year UNIQUE(transect_id, [year])
);
GO

CREATE INDEX idx_notflown_transect_id ON transect_notflown (transect_id);

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table stores instances where a transect segment(s) were missed or omitted from the survey in a current year. Essentially the annual "design" is represented as the all of the records identified in the transect table minus any records listed here for a given year of the survey.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'transect_notflown'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier of a transect segment not flown.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'transect_notflown', @level2type=N'COLUMN',@level2name=N'transect_notflown_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to transect' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'transect_notflown', @level2type=N'COLUMN', @level2name=N'transect_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The survey year in which the transect segment was missed.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'transect_notflown', @level2type=N'COLUMN', @level2name=N'year'
GO


CREATE TABLE [estimate] (
    [estimate_sid] INTEGER IDENTITY(1,1)
  , [species_id] CHAR(4) NOT NULL
  , [year] INTEGER NOT NULL
  , [ecoregion_id] INTEGER  NULL
  , [estimate] DECIMAL(12,6) NOT NULL
  , [variance] DECIMAL(16,6) NOT NULL
  , PRIMARY KEY (estimate_sid)
  , CONSTRAINT fk_estimate_ecoregion
	FOREIGN KEY (ecoregion_id)
	REFERENCES ref_ecoregion(ecoregion)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
  , CONSTRAINT fk_estimate_species 
	FOREIGN KEY (species_id)
	REFERENCES ref_species(species)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
  , CONSTRAINT uc_estimate UNIQUE (species_id, year, ecoregion_id)
);

CREATE INDEX idx_estimate_species ON Estimate (species_id);
CREATE INDEX idx_estimate_year ON Estimate ([year]);
CREATE INDEX idx_estimate_ecoregion ON Estimate (ecoregion_id);

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table store year, species, and stratum (i.e., ecoregion) specific estimates, and the estimate of total ducks across all ecoregions.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier of the estimate.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'estimate_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to species' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'species_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The survey year for which the estimate corresponds too.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to ecoregion. Since this tables stores an estimate for total ducks which corresponds to the entire survey area, its value is NULL.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The estimate reported to 6 decimal places.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'estimate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The svariance of the estimate' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'estimate', @level2type=N'COLUMN',@level2name=N'variance'
GO


CREATE TABLE audit_observation (
	  [audit_observation_sid] INTEGER IDENTITY(1,1)
	, [observation_id] INTEGER 
	, [action_type] CHAR(1) NOT NULL
	, [updated_on] DATETIME NOT NULL
	, [cws_record_identifier] INTEGER NOT NULL
	, [cws_point_identifier] INTEGER NOT NULL
	, [observation_datetime_utc] DATETIME NULL
	, [location_epsg4326] GEOGRAPHY NOT NULL
	, [cws_ecoregion_assignment] INTEGER NOT NULL
	, [cws_transect_assignment] INTEGER NOT NULL
	, [species_id] CHAR(4) NOT NULL
	, [social_group_id] INTEGER NOT NULL
	, [males] INTEGER NULL
	, [females] INTEGER NULL
	, [unknown] INTEGER NULL
)
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table captures the original observation record when the record is updated or deleted in the observation table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An incremental id number to uniquely identify each time observation was updated or removed from the system.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'audit_observation_sid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The observation identifer in the observation table' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'observation_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of action that was taken to result in the record being added to the audit table; U for an update and D for a record that was removed from the database' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'action_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The timestamp in which the record was added to the table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'updated_on'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This original value corresponding to the row number of the observation in the raw data file provided by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'cws_record_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This original value corresponding to an internal code used by CWS, provided in the raw datafile. If this value was absent in the raw datafile, it defaults to the same value at the value in cws_record_identifer.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'cws_point_identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original date in which the observation occurred.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'observation_datetime_utc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original location of the helicopter where the observation was made in WGS84.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'location_epsg4326'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original value of the ecoregion the observation was assigned to by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'cws_ecoregion_assignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original value of the transect the observation was assigned to by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'cws_transect_assignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original species code identifier' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'species_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original social group identifier' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'social_group_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original number of birds in the social grouping that could be identified as males' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'males'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original number of birds in the social grouping that could be identified as females' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'females'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The original number of birds in the social grouping where the sex could not be determined. This primarily applies to monomorphic species where sex cannot be determined unless the bird is in hand' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'audit_observation', @level2type=N'COLUMN',@level2name=N'unknown'
GO
