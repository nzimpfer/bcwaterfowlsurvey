 /*
	This file needs to be run after using ogr2ogr to import all of the spatial objects into
	database. 

	Modify the ecoregion table , such that the ecoregion primary key column 
	corresponds to the ecoregion number assigned by CWS.  This number is
	no longer consecutive, becuase of ecoregions that are no longer surveyed.
	These ecoregions were not included in the database.  
 */

USE [ifw9mb-BCwaterfowlSurvey];  
GO

CREATE SPATIAL INDEX  SPIX_ecoregion_geog_4326 on ref_ecoregion(geog_epsg4326) USING geography_auto_grid;


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds spatial polygons and attributes for survey strata.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The stratum number assigned to the ecoregion by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'ecoregion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the ecoregion (strata) as identified by CWS.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Habitat type assigned by CWS.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'habitat_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The internal area of the ecoregion in square meters' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'shape_area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The internal area of the ecoregion in acres' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'area_acres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The internal area of the ecoregion in hectares' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'area_hectares'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The internal area of the ecoregion in square kilometers' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'area_sqkm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Well Known Binary (WKB) object, which stores the spatial representation of the polygon' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_ecoregion', @level2type=N'COLUMN', @level2name=N'geog_epsg4326'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table holds 200m  and 5000m transect buffers.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier of buffer', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'transect_buffered'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'he master line number the feature is associated with. CWS assigned each line an incremental number beginning with 1 at the northern most line in the survey area. This value matches the value in ref_transect', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'master_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Each line within an ecoregion was given a line number beginning with 1 for the northern most line in the ecoregion. This value matches the value in ref_transect', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'ecoregion_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Due to the irregular boundaries of the ecoregions, a line may pass through multiple ecoregions. Each segment of the same line within an ecoregion is given a unique identifier. This value matches the value in ref_transect', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'segment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ecoregion number', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the area in square meters of the 200m buffer', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'shape_area_m'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The area in square kilometers of the 200m buffer', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'area_sqkm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Well known binary object which stores the representation of the 200m width buffer in WGS84.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'geog_200m_epsg4326'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Well known binary object which stores the representation of the 5000m width buffer in WGS84.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect_buffered', @level2type=N'COLUMN', @level2name=N'geog_5000m_epsg4326'
GO

/*
	Add doc strings to transect table.
*/
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds spatial lines and attributes of transects flown during the survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A unique identifier of a continuous portion of a transect' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'transect'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The master line number the feature is associated with. CWS assigned each line an incremental number beginning with 1 at the northern most line in the survey area.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'master_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Each line within an ecoregion was given a line number beginning with 1 for the northern most line in the ecoregion.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'ecoregion_line'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Due to the irregular boundaries of the ecoregions, a line may pass through multiple ecoregions. Each segment of the same line within an ecoregion is given a unique identifier.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'segment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ecoregion number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'ecoregion_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The length of the transect in meters.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'length_m'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The length of the transect segement in kilometers.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'length_km'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Well known binary object which stores the representation of the transect line in WGS84.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'ref_transect', @level2type=N'COLUMN', @level2name=N'geog_epsg4326'
GO