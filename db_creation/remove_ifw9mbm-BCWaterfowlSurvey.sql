USE [ifw9mb-BCwaterfowlSurvey];
GO
-- Disable ALL constraints in the DB
EXEC sp_MSforeachtable 'SET QUOTED_IDENTIFIER ON; ALTER TABLE ? NOCHECK CONSTRAINT ALL'

-- Disable ALL triggers
EXEC sp_MSforeachtable 'ALTER TABLE ? DISABLE TRIGGER ALL'

-- Drop ALL Views
DROP VIEW [dbo].[transect_buffer_5000m]
DROP VIEW [dbo].[transect_buffer_200m]
DROP VIEW [dbo].[tib_tip_by_year_species_transect]
DROP VIEW [dbo].[SurveyEffort_by_year]
DROP VIEW [dbo].[Records_with_errors]
DROP VIEW [dbo].[estimates_species_totals]
DROP VIEW [dbo].[all_observations_flat_view]
GO

-- Drop ALL Functions
DROP FUNCTION [dbo].[calculateTIPs]
DROP FUNCTION [dbo].[calculateTIBs]
DROP FUNCTION [dbo].[chk_IsMonomorphic]
GO

-- Drop ALL Stored Procedures
DROP PROCEDURE [dbo].[sp_estimate_upsert]
GO

-- Delete ALL data from ALL tables
EXEC sp_MSforeachtable 'SET QUOTED_IDENTIFIER ON; DELETE FROM ?'

DROP TABLE audit_observation;
DROP TABLE observation_transect_error;
DROP TABLE observation_transect;
DROP TABLE observation;
DROP TABLE transect_notflown;
DROP TABLE Estimate;
GO

DROP TABLE ref_transect;
DROP TABLE ref_transect_buffered;
DROP TABLE ref_species;
DROP TABLE ref_ecoregion;
DROP TABLE ref_social_group;
DROP TABLE geometry_columns;
DROP TABLE spatial_ref_sys;
GO