USE [ifw9mb-BCwaterfowlSurvey];
GO
-- Disable ALL constraints in the DB
EXEC sp_MSforeachtable 'SET QUOTED_IDENTIFIER ON; ALTER TABLE ? NOCHECK CONSTRAINT ALL'

TRUNCATE TABLE audit_observation;
TRUNCATE TABLE observation_transect_error;
TRUNCATE TABLE observation_transect;
TRUNCATE TABLE observation;

-- re-enable all constraints
EXEC sp_MSforeachtable 'SET QUOTED_IDENTIFIER ON; ALTER TABLE ? CHECK CONSTRAINT ALL'
EXEC sp_MSforeachtable 'SET QUOTED_IDENTIFIER ON;'

-- Reset all identity keys to 1
-- Will report the current value, not that its been reset to a value of 1.

DBCC CHECKIDENT ('audit_observation', RESEED, 0)
DBCC CHECKIDENT ('observation', RESEED, 0)
DBCC CHECKIDENT ('observation_transect_error', RESEED, 0)
