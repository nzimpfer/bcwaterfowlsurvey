/*
	Upsert Procedure for estimates.
	
	Annually we generate esimates each year by species [group] and ecoregion. However periodically data may get changed or 
	updated, or estimation procedure chanGed, and the estimates for a particular year or the time series may get updated.
	Rather than manually trying to detemine which records were update; this procedure does a formal upsert.  If the records
	exist already they are updated, if not they are inserted.
*/
CREATE PROCEDURE sp_estimate_upsert (@species CHAR(4), @year INT, @ecoregion INT, @estimate FLOAT, @variance FLOAT)
AS 
BEGIN
    MERGE dbo.Estimate WITH (HOLDLOCK) AS dtarget
    USING (SELECT UPPER(@species) AS species_id
				, @year AS [year]
				, @ecoregion AS ecoregion_id
				, @estimate AS estimate
				, @variance as variance) AS dsource
        ON dsource.species_id = dtarget.species_id AND dsource.year = dtarget.year AND dsource.ecoregion_id = dtarget.ecoregion_id
    WHEN MATCHED THEN UPDATE
        SET dtarget.estimate = dsource.estimate, dtarget.variance = dsource.variance
    WHEN NOT MATCHED THEN 
        INSERT ([species_id], [year], [ecoregion_id], [estimate], [variance]) 
        VALUES (UPPER(@species), @year, @ecoregion, @estimate, @variance);
END
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annually we generate esimates each year by species [group] and ecoregion. However periodically data may get changed or updated, or estimation procedure chanGed, and the estimates for a particular year or the time series may get updated. Rather than manually trying to detemine which records were update; this procedure does a formal upsert.  If the records exist already they are updated, if not they are inserted.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'PROCEDURE', @level1name=N'sp_estimate_upsert'
GO


/*
	Create a stored procedure that performs basic validity testing of columns
	on the raw data submitted to FWS prior to adding it to the observation table.

	The insert trigger on the observation table will handle the more complex spatial errors.
*/
CREATE TYPE CWS_RawFeed_TableType AS TABLE (
	  [rec_id] INTEGER NOT NULL
	, [point_id] VARCHAR(8) NULL
	, [date] DATETIME NULL	
	, [longitude] FLOAT NULL
	, [latitude] FLOAT NULL
	, [species] VARCHAR(10) NULL
	, [bird_on_nest] INTEGER NULL
	, [pair] INTEGER NULL
	, [lone_m] INTEGER NULL
	, [lone_f] INTEGER NULL
	, [male2] INTEGER NULL
	, [male3] INTEGER NULL
	, [male4] INTEGER NULL
	, [mixed_m] INTEGER NULL
	, [mixed_f] INTEGER NULL
	, [unknown_sex] INTEGER NULL
	, [ecoregion] VARCHAR(100) NULL
	, [line_id] INTEGER NULL
	, [location] GEOGRAPHY
);
GO

CREATE PROCEDURE sp_cws_raw_data_check (@TVP dbo.CWS_RawFeed_TableType READONLY)
AS
BEGIN
	SET NOCOUNT ON;	
	DECLARE @RawDataErrors TABLE (
		  [error_type] VARCHAR(500) NOT NULL
		, [rec_id] INTEGER NOT NULL
		, [point_id] VARCHAR(8) NULL
		, [date] DATE NULL
		, [longitude] FLOAT NULL
		, [latitude] FLOAT NULL
		, [species] VARCHAR(10) NULL
		, [bird_on_nest] INTEGER NULL
		, [pair] INTEGER NULL
		, [lone_m] INTEGER NULL
		, [lone_f] INTEGER NULL
		, [male2] INTEGER NULL
		, [male3] INTEGER NULL
		, [male4] INTEGER NULL
		, [mixed_m] INTEGER NULL
		, [mixed_f] INTEGER NULL
		, [unknown_sex] INTEGER NULL
		, [ecoregion] VARCHAR(100) NULL
		, [line_id] INTEGER NULL		
	);

	-- Invalid species code
	INSERT INTO @RawDataErrors
	SELECT 'Invalid species code' AS error_type
		, a.rec_id
		, a.point_id
		, a.date
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	WHERE NOT EXISTS (SELECT * FROM ref_species WHERE [species] = ref_Species.[species])

	-- Count value not present in any column
	INSERT INTO @RawDataErrors	
	SELECT 'Count value not present in any column' AS error_type
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	WHERE ([bird_on_nest] = 0 OR [bird_on_nest] IS NULL) 
		AND ([pair] = 0 OR [pair] IS NULL) 
		AND ([lone_m] = 0 OR [lone_m] IS NULL) 
		AND ([lone_f] = 0 OR [lone_f] IS NULL) 
		AND ([male2] = 0 OR [male2]IS NULL) 
		AND ([male3] = 0 OR [male3]IS NULL) 
		AND ([male4] = 0 OR [male4] IS NULL) 
		AND ([mixed_m] = 0 OR [mixed_m] IS NULL) 
		AND ([mixed_f] = 0 OR [mixed_f] IS NULL) 
		AND ([unknown_sex] = 0 OR [unknown_sex] IS NULL) 

	-- Invalid ecoregion
	INSERT INTO @RawDataErrors
	SELECT 'Invalid ecoregion' AS error_type
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	WHERE NOT EXISTS (SELECT * FROM ref_ecoregion WHERE a.[ecoregion] = ref_ecoregion.[description])

	-- Invalid social group for species	
	--		monomorphics can only have unknown sex, thus if that column is 0 or null than the
	--		count must be in other columns and incorrect.
	INSERT INTO @RawDataErrors
	SELECT 'Invalid social group for species' AS error_type 
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	INNER JOIN ref_Species
		ON a.species = ref_Species.species
	WHERE (unknown_sex = 0 OR unknown_sex IS NULL) 
		AND is_monomorphic = 1

	-- bird on nest is limited to a few species. The sppecies listed 
	-- here are those found in the existing data through 2018.
	INSERT INTO @RawDataErrors
	SELECT 'Invalid social group for species' AS error_type 
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	WHERE [bird_on_nest] IS NOT NULL AND species NOT IN ('CAGO','TRUS','RNGR','SACR','COLO');

	-- Missing or invalid (fall outside of study area bounding box) GPS coordinates
	-- calculate bounding box
	DECLARE @G GEOGRAPHY;
	SELECT @G = geography::UnionAggregate(geog_epsg4326) FROM ref_ecoregion

	DECLARE @boundingbox geometry = geometry::STGeomFromWKB(@G.STAsBinary(), @G.STSrid).STEnvelope();

	DECLARE @studyarea_bbox GEOGRAPHY;
	
	SELECT @studyarea_bbox = geography::STGeomFromWKB(@boundingbox.STAsBinary(), @boundingbox.STSrid);

	-- no intersect new obs with that bounding box.  Should catch those that are really bad.
	INSERT INTO @RawDataErrors
	SELECT 'Missing or invalid GPS coordinates' AS error_type 
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	WHERE a.[location].STIntersects(@studyarea_bbox) = 0

	-- line_id not valid for ecoregion
	INSERT INTO @RawDataErrors
	SELECT 'line_id not valid for ecoregion' AS error_type 
		, a.rec_id
		, a.point_id
		, a.date		
		, a.longitude
		, a.latitude
		, a.species
		, a.bird_on_nest
		, a.pair
		, a.lone_m
		, a.lone_f
		, a.male2
		, a.male3
		, a.male4
		, a.mixed_m
		, a.mixed_f
		, a.unknown_sex
		, a.ecoregion
		, a.line_id
	FROM @TVP a
	LEFT JOIN ref_ecoregion b
		ON a.ecoregion = b.[description]
	LEFT JOIN ref_transect_buffered c
	ON a.line_id = c.ecoregion_line AND b.ecoregion = c.ecoregion_id
	WHERE c.transect_buffered IS NULL

	-- return @RawDataErrors if any exist OR print data are clean.
	IF ((SELECT COUNT(*) from @RawDataErrors) = 0)
		PRINT 'Data are clean. Ready for insertion.'
	ELSE	
		SELECT * FROM @RawDataErrors
		ORDER BY rec_id
END
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This procedure performs a host of data checking on the raw data feed provided by CWS. Procedure returns rows with errors and a description of the error or a success message indicating that the data are ready to be entered in the observation table.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'PROCEDURE', @level1name=N'sp_cws_raw_data_check'
GO

CREATE PROCEDURE usp_insert_observation (@TVP dbo.CWS_RawFeed_TableType READONLY)
AS
BEGIN
/*
	Unpivot the raw CWS data from wide format to long format for inserting into the
	observation table.  This ensures that there is only a single observation per row. 

	Once the long format is created, join the location, datetime column from the raw data,
	and the social_grouping_id from the social_group table.

	Finally, insert the resulting table into the observation table which will perform all
	check constraints.

	Try/Catch block pulled from 
	https://stackoverflow.com/questions/1749719/sql-server-transactions-roll-back-on-error
*/
BEGIN TRY
	BEGIN TRANSACTION;

	WITH long_data_format (rec_id, species, social_group, females, males, unknown) AS (
		/*
			coalesce common columns between the male and female unpivot to create a 
			unique set of columns that can be unioned to unknown and mixed sex groupings.

			NOTE: ensure that join contains a rec_id for all of the initial records (ie., we have
			a record for every record that andre sent.
		*/
		SELECT COALESCE(a.rec_id, b.rec_id) as rec_id
			, COALESCE(a.species, b.species) AS species
			, COALESCE(a.social_group, b.social_group) AS social_group
			, a.males
			, b.females
			, NULL AS unknown
		FROM(
			-- unpivot male sex groupings
			SELECT rec_id
				, species
				, social_group
				, males
			FROM
				(
				SELECT rec_id, species, pair, lone_m, male2, male3, male4
				FROM @TVP
				) AS source
			UNPIVOT
				(
				males FOR social_group IN (pair, lone_m, male2, male3, male4)
				) AS pm
		) a
		FULL OUTER JOIN (
			SELECT rec_id
				, species
				, social_group
				, females
			FROM
				(
				-- unpivot female sex groupings
				SELECT rec_id, species, pair, lone_f, bird_on_nest
				FROM @TVP
				) AS source
			UNPIVOT
				(
				females FOR social_group IN (pair, lone_f, bird_on_nest)
				) AS pf
		) b
		ON a.rec_id = b.rec_id AND a.social_group = b.social_group
		UNION
			-- add in unknown sex groups
			SELECT rec_id
				, species
				, 'unknown_sex_group' AS social_group
				, NULL AS males
				, NULL AS females
				, unknown_sex AS unknown
			FROM @TVP
			WHERE unknown_sex IS NOT NULL OR unknown_sex > 0
		UNION
			-- add mixed sex groups
			SELECT rec_id
				, species
				, 'mixed_sex_group' as social_group
				, mixed_m AS males
				, mixed_f AS females
				, NULL AS unknown
			FROM @TVP
			WHERE mixed_m > 0 or mixed_f > 0
	)
	INSERT INTO observation (cws_record_identifier, cws_point_identifier, observation_datetime_utc,
		species_id, social_group_id, males, females, unknown, location_epsg4326)
	SELECT 
		  base_table.[rec_id]
		, base_table.[point_id]
		, base_table.[date] 
		, long_data_format.species
		, long_data_format.social_group
		, long_data_format.males
		, long_data_format.females
		, long_data_format.unknown
		, base_table.[location]
	FROM long_data_format
	LEFT JOIN (
		SELECT rec_id, point_id, [date], location
		FROM @TVP
		) base_table
	ON long_data_format.rec_id = base_table.rec_id
	LEFT JOIN ref_social_group sg
		ON long_data_format.social_group = sg.cws_column_name;

	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRAN

	DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
	DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
	DECLARE @ErrorState INT = ERROR_STATE()

	-- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );
END CATCH
END
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Unpivot the raw CWS data from wide format to long format for inserting into the observation table.  This ensures that there is only a single observation per row. Once the long format is created, join the location, datetime column from the raw data, and the social_grouping_id from the social_group table. Finally, insert the resulting table into the observation table which will perform all check constraints.' , @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'PROCEDURE', @level1name=N'usp_insert_observation'
GO