USE [ifw9mb-BCwaterfowlSurvey]
GO

SET IDENTITY_INSERT dbo.ref_Social_group ON;
GO

INSERT INTO ref_social_group ([social_group], [description], [cws_column_name]) VALUES
(1,'Pair', 'pair'),
(2,'2 male group', 'm2'),
(3,'3 male group', 'm3'),
(4,'4 male group', 'm4'),
(5,'lone male', 'lone_m'),
(6,'lone female', 'lone_f'),
(7,'mixed sex group', 'mixed_sex_group'),
(8,'indeterminante sex group', 'unknown_sex_group'),
(9,'bird on nest', 'bird_on_nest')
;
GO

SET IDENTITY_INSERT dbo.ref_Social_group OFF;
GO

INSERT INTO ref_species (species, aou_alpha, aou_number, fws_alpha_grouped, common_name, is_Waterfowl, is_monomorphic, is_included_todu, itis_code)
VALUES
('ABDU','ABDU',1330,'ABDU','American black duck',1,1,0,175068),
('AGWT','AGWT',1390,'AGWT','American green-winged teal',1,0,1,175081),
('GWTE','AGWT',1390,'AGWT','American green-winged teal',1,0,1,175081),
('AMCO','AMCO',2210,'AMCO','American coot',1,1,0,176292),
('AMWI','AMWI',1370,'AMWI','American wigeon',1,0,1,175094),
('AWMI','AMWI',1370,'AMWI','American wigeon',1,0,1,175094),
('BUFF','BUFF',1530,'BUFF','Bufflehead',1,0,1,175145),
('BWTE','BWTE',1400,'BWTE','Blue-winged teal',1,0,1,175086),
('CAGO','CAGO',1720,'CAGO','Canada goose',1,0,0,174999),
('CANV','CANV',1470,'CANV','Canvasback',1,0,1,175129),
('CITE','CITE',1410,'CITE','Cinnamon teal',1,0,1,175089),
('DCCO','DCCO',1200,'DCCO','Double-crested cormorant',0,1,0,174717),
('COEI','COEI',1590,'EIDE','Common eider',1,0,0,175155),
('EIDE','EIDE',1599,'EIDE','Eiders',1,0,0,NULL),
('EUWI','EUWI',1360,'EUWI','Eurasian wigeon',1,1,0,175092),
('KIEI','KIEI',1620,'EIDE','King eider',1,0,0,175160),
('SPEI','SPEI',1580,'EIDE','Spectacled eider',1,0,0,175161),
('STEI','STEI',1570,'EIDE','Steller''s eider',1,0,0,175153),
('GADW','GADW',1350,'GADW','Gadwall',1,0,1,175073),
('BAGO','BAGO',1520,'GOLD','Barrow''s goldeneye',1,0,1,175144),
('COGO','COGO',1510,'GOLD','Common goldeneye',1,0,1,175141),
('GOLD','GOLD',1519,'GOLD','Goldeneyes',1,0,1,175140),
('EAGR','EAGR',40,'GREB','Eared grebe',0,1,0,174485),
('GREB','GREB',69,'GREB','Grebes',0,1,0,174477),
('GWFG','GWFG',1710,'GWFG','Greater white-fronted goose',1,1,0,175020),
('WFGO','GWFG',1710,'GWFG','Greater white-fronted goose',1,1,0,175020),
('HOGR','HOGR',30,'GREB','Horned grebe',0,1,0,174482),
('PBGR','PBGR',60,'GREB','Pied-billed grebe',0,1,0,174505),
('RNGR','RNGR',20,'GREB','Red-necked grebe',0,1,0,174479),
('HARD','HARD',1550,'HARD','Harlequin duck',1,0,0,175149),
('COLO','COLO',70,'LOON','Common loon',1,1,0,174469),
('LOON','LOON',799,'LOON','Loons',0,1,0,174468),
('PALO','PALO',100,'LOON','Pacific loon',0,1,0,174475),
('RTLO','RTLO',110,'LOON','Red throated loon',0,1,0,174474),
('LESN','LSGO',1690,'LSGO','Snow goose',1,1,0,175038),
('SNGO','LSGO',1690,'LSGO','Snow goose',1,1,0,175038),
('LSGO','LSGO',1690,'LSGO','Snow goose',1,1,0,175038),
('LTDU','LTDU',1540,'LTDU','Long-tailed duck',1,0,0,175147),
('OLDS','LTDU',1540,'LTDU','Long-tailed duck',1,0,0,175147),
('MALL','MALL',1320,'MALL','Mallard',1,0,1,175063),
('COME','COME',1290,'MERG','Common merganser',1,0,0,175185),
('HOME','HOME',1310,'MERG','Hooded merganser',1,0,0,175182),
('MERG','MERG',1299,'MERG','Mergansers',1,0,0,NULL),
('RBME','RBME',1300,'MERG','Red brested merganser',1,0,0,175187),
('NOPI','NOPI',1430,'NOPI','Northern pintail',1,0,1,175074),
('NOSH','NSHO',1420,'NSHO','Northern shoveler',1,0,1,175096),
('NOSL','NSHO',1420,'NSHO','Northern shoveler',1,0,1,175096),
('NSHO','NSHO',1420,'NSHO','Northern shoveler',1,0,1,175096),
('AWPE','AWPE',1250,'PELI','American white pelican',0,1,0,174684),
('BRPE','BRPE',1260,'PELI','Brown pelican',0,1,0,174685),
('PELI','PELI',1299,'PELI','Pelicans',0,1,0,174683),
('REDH','REDH',1460,'REDH','Redhead',1,0,1,175125),
('RNDU','RNDU',1500,'RNDU','Ring-necked duck',1,0,1,175128),
('RUDU','RUDU',1670,'RUDU','Ruddy duck',1,0,1,175175),
('SACR','SACR',2060,'SACR','Sandhill Crane',0,1,0,202247),
('GRSC','GRSC',1480,'SCAU','Greater scaup',1,0,1,175130),
('LESC','LESC',1490,'SCAU','Lesser scaup',1,0,1,175134),
('SCAU','SCAU',1499,'SCAU','Scaup',1,0,1,NULL),
('BLSC','BLSC',1630,'SCOT','Black Scoter',1,0,0,175171),
('SCOT','SCOT',1639,'SCOT','Scoters',1,0,0,175162),
('SUSC','SUSC',1660,'SCOT','Surf scoter',1,0,0,175170),
('WWSC','WWSC',1650,'SCOT','White-winged scoter',1,0,0,175163),
('MUSW','MUSW',1782,'SWAN','Mute swan',1,1,0,174985),
('SWAN','SWAN',1809,'SWAN','Swans',1,1,0,174984),
('TRUS','TRUS',1810,'SWAN','Trumpeter swan',1,1,0,174992),
('TRSW','TRUS',1810,'SWAN','Trumpeter swan',1,1,0,174992),
('TODU','TODU', 0,'TODU','Total Ducks',0,0,0,NULL),
('TUSW','TUSW',1800,'SWAN','Tundra swan',1,1,0,174987),
('WODU','WODU',1440,'WODU','Wood duck',1,0,0,175122);
GO