USE [ifw9mb-BCwaterfowlSurvey]
GO
/*
	Test that the trigger which performs the spatial join when records are updated
	in the observation table.

	TEST CASES:
	1. A valid record @5000m buffer is updated to be valid @200m	

	2. An invalid record remains invalid when ecoregion is updated @200m
	3. An invalid record is now valid when ecoregion is updated @200m validated buffer

	4. An invalid record remains invalid when transect is updated @200m
	5. An invalid record is now valid when transect is updated @200m validated buffer

	6. An invalid record remains invalid when ecoregion is updated @5000m
	7. An invalid record is now valid when ecoregion is updated @5000m validated buffer
	
	8. An invalid record remains invalid when transect is updated @5000m
	9. An invalid record is now valid when transect is updated @5000m validate buffer	

	10. An invalid record remains invalid when location is updated outside all transects	
	11. A record that falls outside of all transects is now valid when location updated @200m validated buffer
	12. A record that falls outside of all transects is now valid when location updated @5000m validated buffer
*/
SET IDENTITY_INSERT observation ON;
INSERT INTO Observation (
	  [Observation_sid]
	, [cws_record_identifier]
	, [cws_point_identifier]
	, [observation_datetime_utc]
	, [location_epsg4326]
	, [cws_ecoregion_assignment]
	, [cws_transect_assignment]
	, [species_id]
	, [social_group_id]
	, [males]
	, [females]
	, [unknown]
)
VALUES
/* insert some valid and some invalid records*/
(1,1,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-121.69856 51.72058)',4326), 9, 6,'MALL',5,2,NULL,NULL), -- valid 5000m
(2,2,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.44740 53.88570)',4326), 1, 10,'MALL',5,2,NULL,NULL), -- bad ecoregion @200m (correct 2,10)
(3,3,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.44740 53.88570)',4326), 2, 6,'MALL',5,2,NULL,NULL), -- bad transect @200m (correct 2,10)
(4,4,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.69780 53.88720)',4326), 1, 10,'MALL',5,2,NULL,NULL), -- bad ecoregion @5000m (correct 2,10)
(5,5,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.69780 53.88720)',4326), 2, 6,'MALL',5,2,NULL,NULL), -- bad transect @5000m (correct 2,10)
(6,6,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(0.0000 0.0000)',4326), 2, 10,'MALL',5,2,NULL,NULL), -- bad location outside of 5000m buffer
(7,7,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.7546 53.9552)',4326), 2, 10,'MALL',5,2,NULL,NULL), -- bad location outside of 5000m buffer
(8,8,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-123.7546 53.9552)',4326), 2, 10,'MALL',5,2,NULL,NULL); -- bad location outside of 5000m buffer
GO
SET IDENTITY_INSERT observation OFF;

/*
	verify that data are as expecteted in observation_transect & observation_transect_error

	observation_transect
	1, 97, 5000

	observation_transect_error.  The order of insertion is not garunteed, thus key is not known.
	--,2,ERROR_differing_line_assignment_200m
	--,3,ERROR_differing_line_assignment_200m
	--,4,ERROR_differing_line_assignment_5000m
	--,5,ERROR_differing_line_assignment_5000m
	--,6,ERROR_Observation_outside_all_buffered_transects
	--,7,ERROR_Observation_outside_all_buffered_transects
	--,8,ERROR_Observation_outside_all_buffered_transects
*/
SELECT * FROM observation_transect;
SELECT * FROM observation_transect_error;

/*
	Now perform some update tests and validate results
*/
-- (1) TEST update valid record @5000m buffer to be valid @200m buffer
UPDATE observation SET location_epsg4326 = GEOGRAPHY::STPointFromText('POINT(-121.4866	51.7105)',4326)
where observation_sid = 1;

DECLARE @id INT = 1;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	1,97, 200
	EMPTY SET
*/


-- (2) TEST update record with bad ecoregion to still have a bad ecoregion @200m buffer accuracy
UPDATE observation SET cws_ecoregion_assignment = 3
WHERE observation_sid = 2;

DECLARE @id INT = 2;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return 
	EMPTY SET
	--,2,ERROR_differing_line_assignment_200m
*/


-- (3) TEST update record with bad ecoregion to now be valid @200m buffer accuracy
UPDATE observation SET cws_ecoregion_assignment = 2
WHERE observation_sid = 2;

DECLARE @id INT = 2;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	2,27,200
	EMPTY SET
*/


-- (4) TEST update record with bad transect to still have bad transect @200m buffer accuracy
UPDATE observation SET cws_transect_assignment = 8
WHERE observation_sid = 3;

DECLARE @id INT = 3;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	EMPTY SET
	--,3,ERROR_differing_line_assignment_200m
*/


-- (5) TEST update record with bad transect to now be valid @200m buffer accuracy
UPDATE observation SET cws_transect_assignment = 10
WHERE observation_sid = 3;

DECLARE @id INT = 3;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	3,27,200
	EMPTY SET
*/

-- (6) TEST update record with bad ecoregion to still have bad ecoregion @5000m buffer
UPDATE observation SET cws_ecoregion_assignment = 3
WHERE observation_sid = 4;

DECLARE @id INT = 4;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	EMPTY SET
	--,4,ERROR_differing_line_assignment_5000m
*/


-- (7) TEST update record with bad ecoregion to now be valid @5000m buffer
UPDATE observation SET cws_ecoregion_assignment = 2
WHERE observation_sid = 4;

DECLARE @id INT = 4;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	4,27,5000
	EMPTY SET
*/

-- (8) TEST update record with bad transect to still have bad transect @5000m buffer
UPDATE observation SET cws_transect_assignment = 6
WHERE observation_sid = 5

DECLARE @id INT = 5;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	EMPTY SET
	--,5,ERROR_differing_line_assignment_5000m
*/



-- (9) TEST update record with bad transect to now be valid @5000m buffer
UPDATE observation SET cws_transect_assignment = 10
WHERE observation_sid = 5

DECLARE @id INT = 5;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	5,27,5000
	EMPTY SET
*/


-- (10) TEST update record with bad location to new location outside of buffer, sill  bad record
UPDATE observation SET location_epsg4326 = GEOGRAPHY::STPointFromText('POINT(-123.7546 53.9552)', 4326)
WHERE observation_sid = 6;

DECLARE @id INT = 6;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	EMPTY SET
	--, 6,ERROR_ERROR_Observation_outside_all_buffered_transects
*/


-- (11) TEST update record with bad location to new location INSIDE 5000m buffer
UPDATE observation SET location_epsg4326 = GEOGRAPHY::STPointFromText('POINT(-123.7770 53.9037)', 4326)
WHERE observation_sid = 7;

DECLARE @id INT = 7;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	7,27,5000
	EMPTY SET
*/


-- (12) TEST update record with bad location to new location INSIDE 200m buffer
UPDATE observation SET location_epsg4326 = GEOGRAPHY::STPointFromText('POINT(-123.83034 53.88497)', 4326)
WHERE observation_sid = 8;

DECLARE @id INT = 8;
SELECT * FROM observation_transect WHERE observation_id = @id;
SELECT * FROM observation_transect_error WHERE observation_id = @id;
/*
	should return
	8,27,200
	EMPTY SET
*/



-- Reset the database 
DELETE FROM observation;
DELETE FROM observation_transect
DELETE FROM observation_transect_error
DELETE FROM audit_observation;

DBCC CHECKIDENT('observation_transect_error',RESEED,0);
DBCC CHECKIDENT('observation', RESEED, 0);
DBCC CHECKIDENT('audit_observation', RESEED, 0);