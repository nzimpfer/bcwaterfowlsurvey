/*
	Test the procedure that performs data quality checks on CWS raw data.
*/
CREATE TABLE #TestData (
	  [rec_id] INTEGER NOT NULL
	, [point_id] VARCHAR(8) NULL
	, [date] VARCHAR(50) NULL
	, [year] INTEGER NULL
	, [month] INTEGER NULL
	, [day] INTEGER NULL
	, [time] VARCHAR(50) NULL
	, [longitude] FLOAT NULL
	, [latitude] FLOAT NULL
	, [species] VARCHAR(10) NULL
	, [bird_on_nest] INTEGER NULL
	, [pair] INTEGER NULL
	, [lone_m] INTEGER NULL
	, [lone_f] INTEGER NULL
	, [male2] INTEGER NULL
	, [male3] INTEGER NULL
	, [male4] INTEGER NULL
	, [mixed_m] INTEGER NULL
	, [mixed_f] INTEGER NULL
	, [unknown_sex] INTEGER NULL
	, [ecosection] VARCHAR(100) NULL
	, [line_id] INTEGER NULL
	, [location] GEOGRAPHY
);

-- create some data 
INSERT INTO #TestData (rec_id, point_id, date, year, month, day, time, latitude, longitude, ecosection, line_id, species, 
	pair, bird_on_nest, lone_m, lone_f, male2, male3, male4, mixed_m, mixed_f, unknown_sex)
VALUES
(3,3,'5/1/2018',2018,5,1,'13:28:11',51.129233,-120.772551,'Cariboo Basin',20,'mall',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL), -- line in ecosection
(4,4,'5/1/2018',2018,5,1,'13:28:13',51.129529,-120.772528,'Cariboo Basin',10,'mall',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), -- count values
(5,5,'5/1/2018',2018,5,1,'13:28:25',51.130996,-120.769729,'Cariboo Lake',10,'mall',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), -- ecosection
(6,6,'5/1/2018',2018,5,1,'13:28:33',51.130414,-120.767853,'Cariboo Basin',10,'mall',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(7,7,'5/1/2018',2018,5,1,'13:28:37',51.130914,-120.767861,'Cariboo Basin',10,'mall',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), -- invalid social group
(8,8,'5/1/2018',2018,5,1,'13:28:37',51.130914,-120.767861,'Cariboo Basin',10,'agwt',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(9,9,'5/1/2018',2018,5,1,'13:29:14',51.130727,-120.773945,'Cariboo Basin',10,'mall',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,10,'5/1/2018',2018,5,1,'13:29:25',51.129105,-120.774193,'Cariboo Lake',10,'mall',7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), -- ecosection
(11,11,'5/1/2018',2018,5,1,'13:29:29',51.128541,-120.774312,'Cariboo Basin',10,'nopi',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,12,'5/1/2018',2018,5,1,'13:29:29',51.128541,-120.774312,'Cariboo Basin',10,'agwt',4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,13,'5/1/2018',2018,5,1,'13:29:29',51.128541,-120.774312,'Cariboo Basin',10,'duck',4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), -- species
(14,14,'5/1/2018',2018,5,1,'13:29:34',51.127938,-120.774744,'Cariboo Basin',10,'mall',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(15,15,'5/1/2018',2018,5,1,'13:29:34',51.127938,-120.774744,'Cariboo Basin',10,'cago',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4),
(16,16,'5/1/2018',2018,5,1,'13:29:34',51.127938,-120.774744,'Cariboo Basin',10,'rndu',5,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL,NULL),
(17,17,'5/1/2018',2018,5,1,'13:30:05',31.130206,-120.777114,'Cariboo Basin',10,'mall',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);  -- gps
GO

-- Create geography 
UPDATE #TestData
	SET location = GEOGRAPHY::STPointFromText('POINT(' + CAST(longitude AS varchar) + ' ' + CAST(latitude AS varchar) + ')', 4326)
GO

/*
	Run procedure 
	The follown needs to be run in a single batch

	RETURN TABLE
	'line_id not valid for ecoregion', 3
	'Count value not present in any column', 4
    'Invalid ecoregion', 5
	'line_id not valid for ecoregion', 5
	'Invalid social group for species', 7
    'Invalid ecoregion', 10
	'line_id not valid for ecoregion', 10
    'invalid species', 13
    'Missing or invalid GPS coordinates', 17
*/
SET NOCOUNT ON;
DECLARE @TTable AS CWS_RawFeed_TableType;

INSERT INTO @TTable
SELECT rec_id, point_id, CAST(date + ' ' + time AS datetime), longitude, latitude, species, bird_on_nest,
	pair, lone_m, lone_f, male2, male3, male4, mixed_m, mixed_f, unknown_sex, ecosection, line_id, location
FROM #TestData

EXEC sp_cws_raw_data_check @TTable;
GO

/*
	Run procedure on data with no errors 
	to test return message.
*/
SET NOCOUNT ON;
DECLARE @TTable AS CWS_RawFeed_TableType;

INSERT INTO @TTable
SELECT rec_id, point_id, CAST(date + ' ' + time AS datetime), longitude, latitude, species, bird_on_nest,
	pair, lone_m, lone_f, male2, male3, male4, mixed_m, mixed_f, unknown_sex, ecosection, line_id, location
FROM #TestData
WHERE rec_id IN (6,8,9,11,12,14,15,16);

EXEC sp_cws_raw_data_check @TTable;
GO

-- END TEST; CLEAN UP
DROP TABLE #TestData