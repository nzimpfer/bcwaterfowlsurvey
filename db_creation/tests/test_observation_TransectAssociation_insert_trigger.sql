USE [ifw9mb-BCwaterfowlSurvey]
GO

/*
	Test that the trigger which performs the spatial join when new records are inserted 
	into the observation table.

	test values derived from 2006 data record_id = 1459, 2077, ?, 1075, 1011
*/
SET IDENTITY_INSERT observation ON;
INSERT INTO Observation (
	  [Observation_sid]
	, [cws_record_identifier]
	, [cws_point_identifier]
	, [observation_datetime_utc]
	, [location_epsg4326]
	, [cws_ecoregion_assignment]
	, [cws_transect_assignment]
	, [species_id]
	, [social_group_id]
	, [males]
	, [females]
	, [unknown]
)
VALUES
 (1,1459,'363','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-121.4866	51.7105)',4326), 9,6,'MALL',5,2,NULL,NULL) -- valid 200m
,(2,2486,'13','1900-05-24',GEOGRAPHY::STPointFromText('POINT(-125.7919	54.468)',4326), 1,9,'AGWT',1,1,1,NULL) -- valid 5000m
,(3,1075,'1','1900-05-05',GEOGRAPHY::STPointFromText('POINT(-121.7697 51.419)',4326), 2,4,'CAGO',2,2,NULL,NULL) -- differing ecoregion assignment 200m buffer
,(4,1011,'2','1900-05-05',GEOGRAPHY::STPointFromText('POINT(-125.6565 54.4684)',4326), 2,4,'MALL',1,1,NULL,NULL) -- differing ecoregion assignment 5000m buffer
,(5,1075,'1','1900-05-05',GEOGRAPHY::STPointFromText('POINT(-121.7697 51.419)',4326), 1,4,'CAGO',2,2,NULL,NULL) -- differing line assignment 200m buffer
,(6,1011,'2','1900-05-05',GEOGRAPHY::STPointFromText('POINT(-125.6565 54.4684)',4326), 1,4,'MALL',1,1,NULL,NULL) -- differing line assignment 5000m buffer
,(7,5,'2','1900-05-05',GEOGRAPHY::STPointFromText('POINT(0.0000 0.0000)',4326), 1,5,'MALL',1,2,NULL,NULL) -- outside study area
GO
SET IDENTITY_INSERT observation OFF;

/*
	The following rows shoul be returned from a query to observation_transect:

	1, 97, 200
	2, 13, 5000
*/
SELECT * FROM observation_transect;

/*
	The following rows should be returned from a query to observation_transect_error
	1,3,ERROR_differing_line_assignment_200m
	2,4,ERROR_differing_line_assignment_5000m
	3,5,ERROR_differing_line_assignment_200m
	4,6,ERROR_differing_line_assignment_5000m
	5,7,ERROR_Observation_outside_all_buffered_transects
*/
SELECT * FROM observation_transect_error
ORDER BY observation_id;


-- Reset the database 
DELETE FROM observation;
DELETE FROM observation_transect
DELETE FROM observation_transect_error

DBCC CHECKIDENT('observation_transect_error',RESEED,1);
DBCC CHECKIDENT('observation', RESEED, 1);