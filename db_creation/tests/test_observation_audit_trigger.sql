USE [ifw9mb-BCwaterfowlSurvey]
GO

/*
	Test that the trigger which inserts records into the audit_observation table is functioning correctly.
*/
INSERT INTO Observation (
	  [cws_record_identifier]
	, [cws_point_identifier]
	, [observation_datetime_utc]
	, [location_epsg4326]
	, [cws_ecoregion_assignment]
	, [cws_transect_assignment]
	, [species_id]
	, [social_group_id]
	, [males]
	, [females]
	, [unknown]
)
VALUES
(1,'1','2000-05-05',GEOGRAPHY::STPointFromText('POINT(-125.35211 55.04521)',4326), 1,5,'MALL',2,2,NULL,NULL),
(2,'1','2000-05-05',GEOGRAPHY::STPointFromText('POINT(-125.45211 55.04221)',4326), 1,5,'CAGO',2,2,NULL,NULL),
(3,'2','2000-05-05',GEOGRAPHY::STPointFromText('POINT(-125.32210 55.04321)',4326), 1,5,'RUDU',1,1,NULL,NULL),
(4,'2','2000-05-05',GEOGRAPHY::STPointFromText('POINT(-125.30131 55.04600)',4326), 1,5,'MALL',1,2,NULL,NULL);
GO

-- update species column
UPDATE observation
SET species_id = 'TRUS'
WHERE observation_sid = 3

-- delete record 
DELETE FROM observation
WHERE observation_sid = 2

-- update location column of record ?
UPDATE observation 
SET location_epsg4326 = GEOGRAPHY::STPointFromText('POINT(-125.35011 55.04421)', 4326)
WHERE observation_sid = 1

/*
	 Result set should be 3 rows with observation 
*/
SELECT * FROM audit_observation;


-- Reset the database 
DELETE FROM audit_observation;
DELETE FROM observation;

DBCC CHECKIDENT('audit_observation',RESEED,0);
DBCC CHECKIDENT('observation', RESEED, 0);