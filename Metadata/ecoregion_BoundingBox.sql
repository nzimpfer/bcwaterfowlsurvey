/*
	SQL server doesn't have a built in means of determining the bounding box for an object.  For
	metadata purposes, I want the bounding box which envelopes all of the ecoregions in the survey.

	Essentially, merge/dissolve/or union all of the ecoregions into 1 polygon, convert it to a geometry
	find the evnelope (square) and extract the northwest and southeast corners.
*/

DECLARE @g geography;
SELECT @g =  geography::UnionAggregate(geog_4326) FROM ecoregion

DECLARE @geomenvelope geometry;
SET @geomenvelope = geometry::STGeomFromWKB(@g.STAsBinary(), @g.STSrid).STEnvelope()
  
 SELECT
   @geomenvelope.STPointN(1).STX AS minlong,
   @geomenvelope.STPointN(1).STY AS minlat,
   @geomenvelope.STPointN(3).STX AS maxlong,
   @geomenvelope.STPointN(3).STY AS maxlat;

