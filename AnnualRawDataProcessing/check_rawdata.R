# 

INPUT_FILE = '../RawObservationFiles/RELEASE_InteriorWaterfowlSurvey_May2022.csv'
OUTUPT_FILE = '../InitialLoad/2022_data.csv' 

library(reshape2)
library(odbc)

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

source('error_checks.r')
source('utils.r')

con <- dbConnect(odbc::odbc(), 
                 driver = 'ODBC Driver 17 for SQL Server',
                 server = 'ifwres-mbrd_ops',
                 database = "ifw9mb_BCWaterfowlSurvey",
                 trusted_connection = "yes")

data.df <- read.csv(INPUT_FILE,
                    header = TRUE,
                    stringsAsFactors = FALSE,
                    na.strings = 'na')

validate_header(names(data.df))

# Drop unidentified records
data.df <- data.df[!data.df$Species %in% c('UNDI', 'UNDA', 'UNDU'), ]
errors <- perform_error_check(data.df, con)

# Create a formal date object
date_format <- date_format_chk(data.df$Date)
data.df$newDate <- as.POSIXct( with(data.df, paste(Date, TimeGMT24, sep=' ')), 
                               format = paste(date_format, '%H:%M:%S', sep=' '))


names(data.df)[which(names(data.df) %in% c('Recid', 'Female', 'Male1'))] <- c('cws_record_id','Lone_f','Lone_m')

# Make values in Species column upper case
data.df$Species <- toupper(data.df$Species)

# convert columns that have 0 to NA.  Columns mixed_m and mixed_f are excluded
# from this, since its a paired column.
data.df[,c('Pair','Lone_m','Lone_f','Male2','Male3','Male4','UnknownSex')] <-
  apply(data.df[,c('Pair','Lone_m','Lone_f',
                   'Male2','Male3','Male4','UnknownSex')], 2,
        function(x) ifelse(x == 0, NA, x))

# Convert observations of groups of birds to total numbers of birds
data.df$Male2 <- data.df$Male2 * 2
data.df$Male3 <- data.df$Male3 * 3
data.df$Male4 <- data.df$Male4 * 4

# give each row a unique identifier
data.df$ObjectId <- 1:nrow(data.df)

# Start to transpose the wide to long.  Currently, a row may represent multiple
# observations at a single location.  This is a bad practice.  One row/record
# should represent a single observation at a location. Multiple observations,
# as a single location, should be represented by multiple rows.
#
# Done in 3 parts because of the pairing that exists for mixed_m/mixed_f groups.
males <- reshape2::melt(data.df[,c('ObjectId','cws_record_id','PointId',
                                   'Latitude','Longitude','newDate','Ecosection',
                                   'Line','Species','Pair','Lone_m','Male2','Male3',
                                   'Male4','MixedMale')],
                        id.vars = c('ObjectId','cws_record_id','PointId','Latitude',
                                    'Longitude','Species','newDate','Ecosection',
                                    'Line'),
                        value.name = c('males'))
levels(males$variable)[6] <- 'open'

females <- melt(data.df[,c('ObjectId','cws_record_id','PointId','Latitude',
                           'Longitude','newDate','Ecosection','Line',
                           'Species','Pair','Lone_f','MixedFemale','BirdOnNest')],
                id.vars = c('ObjectId','cws_record_id','PointId','Latitude',
                            'Longitude','Species','newDate','Ecosection','Line'),
                value.name = c('females'))
levels(females$variable)[3] <- 'open'

unknown <- melt(data.df[,c('ObjectId','cws_record_id','PointId','Latitude',
                           'Longitude','newDate','Ecosection','Line',
                           'Species','UnknownSex')],
                id.vars = c('ObjectId','cws_record_id','PointId','Latitude',
                            'Longitude','Species','newDate','Ecosection','Line'),
                value.name = c('unknown'))
levels(unknown$variable)[1] <- 'unknown'

# merge the long_sex specific observations back into a single data frame, with
# columns for each sex type.
long_data <- Reduce(function(...) merge(...,
                                        by = c('ObjectId','cws_record_id',
                                               'PointId','Latitude','Longitude',
                                               'Species','newDate','Ecosection',
                                               'Line','variable'),
                                        all = TRUE),
                    list(males,females,unknown))

# Drop nonsensical records. Those records that have 0 or NA values for counts,
# that were created during the transpose from wide to long format.

# find rows where male/female/unknown are NA and drop
allNAval <- apply(long_data[, c('males','females','unknown')], 1,
                  function(row) all(is.na(row)))

long_data2 <- long_data[!allNAval,]

# Drop bad Open records. Records where mixed_m/mixed_f might contain a 0 and the
# other one is an NA
badOpen <- which(long_data2$variable == 'open' &
                   (long_data2$males == 0 & is.na(long_data2$females)) |
                   (long_data2$females == 0 & is.na(long_data2$males)) |
                   (long_data2$males == 0 & long_data2$females == 0))

badOpen
long_data3 <- long_data2

# Group [open] observations must have 0 in the absence of males or females in
# the group. Currently absence of value is an NA. Make them 0.
long_data3[long_data3$variable == 'open' &
             is.na(long_data3$males), 'males'] <- 0
long_data3[long_data3$variable == 'open' &
             is.na(long_data3$females), 'females'] <- 0

# re-number the record ID since each row now represents a single observation
long_data3$ObjectId <- 1:nrow(long_data3)

# -- Cleanup before export
# clean up column names
names(long_data3)[c(7,10)] <- c('Date','SocialGroup')

# Reorder social groupings to correspond to DB
long_data3$SocialGroup <- factor(long_data3$SocialGroup,
                                 levels= c('Pair','Male2','Male3',
                                           'Male4','Lone_m','Lone_f',
                                           'open','unknown','BirdOnNest'))

# Lat/Long values should be to X decimal places
long_data3[,c(5,4)] <- format(long_data3[,c('Longitude','Latitude')], digits= 9)

# convert the social group factor back to an integer
long_data3$SocialGroup_num <- as.numeric(long_data3$SocialGroup)

# -- Export Data for SQL Server
write.csv(long_data3[,c(1:3,5:4,6:9,14,11:13)], 
          OUTPUT_FILE,
          row.names = FALSE,
          quote = FALSE,
          na = '')

odbc::dbDisconnect(con)
