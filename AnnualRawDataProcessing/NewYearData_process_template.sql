/*
	Script Template for the annual processing of data from CWS.
	This script sould be copied and modified, then committed to version control
	every year new data is added to the system.
*/
USE [ifw9mb-BCWaterfowlSurvey];
GO

CREATE TABLE ##new_year_data (
	  recid INT
	, point_id INT
	, obs_date DATE
	, time_gmt TIME
	, longitude FLOAT
	, latitude FLOAT
	, species CHAR(4)
	, pair INT
	, lone_m INT
	, lone_f INT
	, M2 INT
	, M3 INT
	, M4 INT
	, mixed_m INT
	, mixed_f INT
	, bird_on_nest INT
	, unknown_sex INT
	, location GEOGRAPHY
	, obs_datetime DATETIME
	, cws_ecosection_assignment INTEGER NOT NULL
	, cws_transect_assignment INTEGER NOT NULL
);
GO

CREATE TABLE #segments_not_flown (
	  [year] INT NOT NULL
	, [stratum] INT NOT NULL
	, [transect] INT NOT NULL
	, [segment] INT NOT NULL
)
GO

-- Bulk load data from CWS into a temporary table
-- NOTE: The data and format files need to be in a location that the sqlserver has access to.
--		 Basically, this is the local filesystem, at the moment.
BULK INSERT ##new_year_data
FROM '<path_to_file>'
WITH (
	FORMATFILE = '<path_to_formatfile>',
	FIRSTROW = 2
	);
GO

-- look at the first few records to see that things are kosher
SELECT TOP 10 * FROM ##new_year_data;

-- Make species variable uppercase
UPDATE #new_year_data SET species = UPPER(species);

-- Cast to POSIX datetime variable
UPDATE #new_year_data SET obs_datetime = CAST(obs_date AS DATETIME) + CAST(time_gmt AS DATETIME)

-- Create spatial point form lat/long
UPDATE #new_year_data SET location = geography::STGeomFromText('POINT(' +
		CAST([longitude] AS VARCHAR(20)) + ' ' +
		CAST([latitude] AS VARCHAR(20)) + ')', 4326);
GO

-- The counts in some of the social groups represent the count of the groups, not the 
-- number of birds present. Update to be number of birds present
UPDATE ##new_year_data SET m2 = m2 * 2 WHERE m2 IS NOT NULL;
UPDATE ##new_year_data SET m3 = m3 * 3 WHERE m3 IS NOT NULL;
UPDATE ##new_year_data SET m4 = m4 * 4 WHERE m4 IS NOT NULL;


-- Cast the data into the CWS_RawFeed_TableType
-- this should fail if one on the columns doesn't exists or isn't the right data type.
DECLARE @data AS CWS_RawFeed_TableType;
INSERT INTO @data
	SELECT ? FROM #new_year_data
GO

-- THE PARAMETER IS THE CWS_RAWFEED_TABLE TYPE.. CONVERT THE 
-- TEMP TABLE TO THAT TABLE TYPE THEN USE IN PROCEDURE....
EXEC dbo.sp_cws_raw_data_check(@TVP = ##new_year_data);
GO

EXEC usp_insert_observation(@TVP = ##new_year_data)
GO

-- Done with the raw data, drop temp table.
DROP TABLE ##new_year_data;


-- Add specific transects to transect not flown table via
INSERT INTO #segments_not_flown ([year], [transect],[segment]) 
VALUES 
();
GO


INSERT INTO transect_notflown ([year], [transect_id])
SELECT a.[year], b.[transect] 
FROM #segments_not_flown a 
INNER JOIN ref_transect b
	ON a.transect = b.transect
	AND a.segment = b.segment