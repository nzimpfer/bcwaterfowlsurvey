@echo off
REM  	Author 		Nathan Zimpfer
REM	Date 		2019-03-21
REM	
REM 	Overview  	This is a simple batch script which will use OGR2OGR.EXE to import all of the spatial table.  The script 
REM			takes one argument which is the name of the sql-server to insert the data into. The argument should be 
REM			the short internal name (ifw-dbcsql1), versus the full name (ifw-dbcsqlcl1.fws.doi.net).  This script should
REM			only be run while attached to the FWS network. This script assumes that the data and format files are in 
REM			the same directory as this script. 
REM
REM	Arguments 	sql-server
REM 
REM 	Usage		ogr_insert_SpatialData.bat <sql-server>

if [%1]==[] goto usage
set SERVER=%1

@echo Inserting data into `ref_ecoregion`...
ogr2ogr -f "MSSQLSpatial" "MSSQL:server=%SERVER%;database=ifw9mb-bcwaterfowlsurvey;trusted_connection=yes;" "ecoregion_epsg4326.geojson" -a_srs "EPSG:4326" -lco "GEOM_TYPE=geography" -lco "GEOM_NAME=geog_epsg4326" -lco "FID=ecoregion" -lco "SPATIAL_INDEX=NO" -nln "ref_ecoregion" -overwrite 

@echo Inserting data into `ref_transect` ...
ogr2ogr -f "MSSQLSpatial" "MSSQL:server=%SERVER%;database=ifw9mb-bcwaterfowlsurvey;trusted_connection=yes;" "BC_Transects_epsg4326.geojson" -a_srs "EPSG:4326" -lco "GEOM_TYPE=geography" -lco "GEOM_NAME=geog_epsg4326" -lco "FID=transect" -nln "ref_transect" -overwrite



@echo Inserting data into `ref_transect_buffered` ...
ogr2ogr -f "MSSQLSpatial" "MSSQL:server=%SERVER%;database=ifw9mb-bcwaterfowlsurvey;trusted_connection=yes;" "BC_Transects_polygon_200m_buffer_epsg4326.geojson" -a_srs "EPSG:4326" -lco "GEOM_TYPE=geography" -lco "GEOM_NAME=geog_200m_epsg4326" -lco "FID=transect_buffered" -nln "ref_transect_buffered" -overwrite




@echo Inserting data into `transect_buffer_5000m` ...
ogr2ogr -f "MSSQLSpatial" "MSSQL:server=%SERVER%;database=ifw9mb-bcwaterfowlsurvey;trusted_connection=yes;" "BC_Transects_polygon_5000m_buffer_epsg4326.geojson" -a_srs "EPSG:4326" -lco "GEOM_TYPE=geography" -lco "GEOM_NAME=geog_5000m_epsg4326" -nln "transect_buffer_5000" -overwrite

goto :eof

:usage
@echo Usage: %0 ^<sql-server^>
exit /B 1