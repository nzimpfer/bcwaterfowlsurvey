-- Fix spatial geometries of the imported shapefiles
UPDATE ref_ecoregion 
SET geog_epsg4326 = geog_epsg4326.ReorientObject()
GO

UPDATE ref_transect_buffered
SET geog_200m_epsg4326 = geog_200m_epsg4326.ReorientObject()
GO

UPDATE transect_buffer_5000
SET geog_5000m_epsg4326 = geog_5000m_epsg4326.ReorientObject()
GO


-- Drop column no longer neede from ref_transect
ALTER TABLE ref_transect
DROP COLUMN fid
GO

ALTER TABLE ref_transect_buffered
DROP COLUMN fid
GO

-- add the 5000m buffer geometries to the 200m buffer geometries
ALTER TABLE ref_transect_buffered
ADD geog_5000m_epsg4326 GEOGRAPHY
GO

UPDATE ref_transect_buffered
SET geog_5000m_epsg4326 = b5000.geog_5000m_epsg4326
FROM ref_transect_buffered buff
INNER JOIN transect_buffer_5000 b5000
ON buff.master_line = b5000.masterline AND 
   buff.ecoregion_line = b5000.ecosecline AND 
   buff.ecoregion_id = b5000.ecoregion_ AND
   buff.segment = b5000.segment



-- Drop table no longer needed
DROP TABLE transect_buffer_5000
GO

UPDATE geometry_columns
SET f_table_name = 'ref_transect_buffered'
WHERE f_table_name = 'transect_buffer_5000'

-- Create spatial indexes on the updated tables
CREATE SPATIAL INDEX spidx_200m_buffer_transect ON dbo.ref_transect_buffered(geog_200m_epsg4326)
CREATE SPATIAL INDEX spidx_5000m_buffer_transect ON dbo.ref_transect_buffered(geog_5000m_epsg4326)

-- create FK on spatial tables
ALTER TABLE ref_transect WITH CHECK
ADD CONSTRAINT [fk_transect_ecoregion] FOREIGN KEY ([ecoregion_id])
REFERENCES [dbo].[ref_ecoregion]([ecoregion])
GO

ALTER TABLE ref_transect_buffered WITH CHECK
ADD CONSTRAINT [fk_transect_buffered_ecoregion] FOREIGN KEY ([ecoregion_id])
REFERENCES [dbo].[ref_ecoregion]([ecoregion])
GO