/*
	Upsert Procedure for estimates.
	
	We generate esimates each year by species [group] and ecoregion. However periodically data may get changed or 
	updated, or estimation procedure chagned, and the estimates for a particular year or the time series may get updated.
	Rather than 
*/
CREATE   PROCEDURE [dbo].[sp_estimate_upsert] (@species CHAR(4), @year INT, @ecoregion INT, @estimate FLOAT, @variance FLOAT)
AS 
BEGIN
    MERGE dbo.Estimate WITH (HOLDLOCK) AS dtarget
    USING (SELECT UPPER(@species) AS species_id
				, @year AS [year]
				, @ecoregion AS ecoregion_id
				, @estimate AS estimate
				, @variance as variance) AS dsource
        ON dsource.species_id = dtarget.species_id AND dsource.year = dtarget.year AND COALESCE(dsource.ecoregion_id, -99) = COALESCE(dtarget.ecoregion_id, -99)
    WHEN MATCHED THEN UPDATE
        SET dtarget.estimate = dsource.estimate, dtarget.variance = dsource.variance
    WHEN NOT MATCHED THEN 
        INSERT ([species_id], [year], [ecoregion_id], [estimate], [variance]) 
        VALUES (UPPER(@species), @year, @ecoregion, @estimate, @variance);
END
GO
