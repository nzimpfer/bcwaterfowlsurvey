SELECT
	  o.[observation_sid]
	, o.[observation_datetime_utc]
	, t.ecoregion_id
	, e.[description] as ecoregion
	, t.[ecoregion_line] as transect
	, o.[species_id]
	, s.[common_name]
	, o.[social_group_id]
	, sg.[description] AS social_group_description
	, CASE WHEN o.social_group_id IN (1, 5) THEN males
		WHEN social_group_id = 2 THEN males / 2
		WHEN social_group_id = 3 THEN males / 3
		WHEN social_group_id = 4 THEN males / 4
		WHEN social_group_id IN (6,9) THEN females
		WHEN social_group_id = 7 THEN males + females
		WHEN social_group_id = 8 THEN unknown END AS [count]
	, males
	, females
	, unknown
	, dbo.calculateTIBs(o.species_id, o.social_group_id, o.males, o.females, o.unknown) AS TIBs
	, dbo.calculateTIPs(o.species_id, o.social_group_id, o.males, o.females) AS TIPs
	, o.location_epsg4326.Lat AS latitude
	, o.location_epsg4326.Long AS longitude
	, o.location_epsg4326.STAsText() as location_WKT
	, o.created_at
	, o.updated_at
FROM Observation o
LEFT JOIN ref_species s
	ON O.species_id = s.species
LEFT JOIN ref_social_group sg
	ON o.social_group_id = sg.social_group
LEFT JOIN ref_transect t
	ON o.transect_id = t.transect
LEFT JOIN ref_ecoregion e
	on t.ecoregion_id = e.ecoregion;
