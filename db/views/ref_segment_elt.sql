select
  t.transect,
  t.master_line,
  t.ecoregion_line,
  t.segment,
  t.ecoregion_id,
  t.length_m,
  t.length_km,
  t.geog_epsg4326.STAsText() as line_epsg4326_WKT,
  t.created_at,
  t.updated_at
from ref_transect t
