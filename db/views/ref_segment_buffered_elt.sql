select
  tbuf.transect_buffered,
  tbuf.master_line,
  tbuf.ecoregion_line,
  tbuf.segment,
  tbuf.ecoregion_id,
  tbuf.shape_area_m,
  tbuf.area_sqkm,
  tbuf.geog_200m_epsg4326.STAsText() as polygon_200m_epsg4326_WKT,
  tbuf.created_at,
  tbuf.updated_at
from  ref_transect_buffered tbuf
