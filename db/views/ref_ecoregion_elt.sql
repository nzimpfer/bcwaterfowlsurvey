-- Creat a view for ELT runs.  The main intent
-- is to convert the spatial object to well-known-text (WKT) format
SELECT
	ecoregion,
	geog_epsg4326.STAsText() as polygon_epsg4326_wkt,
	description,
	habitat_type,
	area_hectares,
	area_acres,
	shape_area,
	area_sqkm,
	created_at,
	updated_at
FROM ref_ecoregion
