SELECT
		  [observation_sid]
		, [cws_record_identifier]
		, [cws_point_identifier]
		, YEAR([observation_datetime_utc]) AS [Year]
		, [cws_ecoregion_assignment] AS [cws_assigned_ecoregion_id]
    , e2.description AS [cws_assigned_ecoregion]
		, [cws_transect_assignment] AS [cws_assigned_transect]
    , t.ecoregion_line AS [intersect_assigned_transect]
    , e.[ecoregion] AS [intersect_assigned_ecoregion_id]
    , e.[description] AS [intersect_assigned_ecoregion]
    , o.error
    , o.location_epsg4326.Lat AS Latitude
    , o.location_epsg4326.Long AS Longitude
		, o.location_epsg4326
FROM observation o
LEFT JOIN ref_transect t
  ON o.transect_id = t.transect
LEFT JOIN ref_ecoregion e
 ON t.ecoregion_id = e.ecoregion
LEFT JOIN ref_ecoregion e2
  ON o.cws_ecoregion_assignment = e2.ecoregion
WHERE error IS NOT NULL
