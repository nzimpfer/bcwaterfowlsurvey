/*
	Replicate SQL Server CDC functionality but on a permenant basis.
*/
CREATE OR ALTER TRIGGER TRG_estimate_audit
ON estimate
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @operation_code INT
	DECLARE @operation VARCHAR(20)

	-- insert only
	IF NOT EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @operation_code = 2;
		SET @operation = 'INSERT';
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by],
		[operation_code], [operation], [species_id], [year], [ecoregion_id],
		[estimate], [variance])
		SELECT [estimate_sid],
			CURRENT_TIMESTAMP,
			SYSTEM_USER,
			@operation_code,
			@operation,
			[species_id],
			[year],
			[ecoregion_id],
			[estimate],
			[variance]
		FROM inserted
	END

	-- delete only
	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @operation = 1;
		SET @operation = 'DELETE';
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by],
		[operation_code], [operation], [species_id], [year], [ecoregion_id],
		[estimate], [variance])
		SELECT [estimate_sid],
			CURRENT_TIMESTAMP,
			SYSTEM_USER,
			@operation_code,
			@operation,
			[species_id],
			[year],
			[ecoregion_id],
			[estimate],
			[variance]
		FROM deleted
	END

	-- update (delete followed by insert)
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @operation = 3;
		SET @operation = 'UPDATE-PRIOR';
		INSERT INTO estimate_audit ([estimate_id],[operation_on], [operation_by],
		[operation_code], [operation], [species_id], [year], [ecoregion_id],
		[estimate], [variance])
		SELECT [estimate_sid],
			CURRENT_TIMESTAMP,
			SYSTEM_USER,
			@operation_code,
			@operation,
			[species_id],
			[year],
			[ecoregion_id],
			[estimate],
			[variance]
		FROM deleted

		SET @operation = 4;
		SET @operation = 'UPDATE-NEW';
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by],
		[operation_code], [operation], [species_id], [year], [ecoregion_id],
		[estimate], [variance])
		SELECT [estimate_sid],
			CURRENT_TIMESTAMP,
			SYSTEM_USER,
			@operation_code,
			@operation,
			[species_id],
			[year],
			[ecoregion_id],
			[estimate],
			[variance]
		FROM inserted
	END
END
GO
