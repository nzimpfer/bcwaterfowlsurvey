CREATE TRIGGER trg_updated_at_transect ON ref_transect
AFTER UPDATE
AS
BEGIN
   SET NOCOUNT ON;

   UPDATE dbo.ref_transect
   SET updated_at = CURRENT_TIMESTAMP
   WHERE EXISTS (
     SELECT 1
     FROM inserted i
     WHERE i.transect = ref_transect.transect
   )
END
GO
