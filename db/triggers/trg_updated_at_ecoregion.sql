CREATE TRIGGER trg_updated_at_ecoregion ON ref_ecoregion
AFTER UPDATE
AS
BEGIN
   SET NOCOUNT ON;

   UPDATE dbo.ref_ecoregion
   SET updated_at = CURRENT_TIMESTAMP
   WHERE EXISTS (
     SELECT 1
     FROM inserted i
     WHERE i.ecoregion = ref_ecoregion.ecoregion
   )
END
GO
