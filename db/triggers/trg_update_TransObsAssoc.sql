/*
	This trigger performs a spatial join for each observation when the geometry coordinates,
	cws_ecoregion_assignment, or cws_line_assignment are updated.

	If the update is valid, the observation_transect table transect_id, and validated buffer
	are updated and the record is removed from the observation_transect_error table, if present.

	If the update still results in an error record the observation_transect_error table is
	updated with a new description.

	If the update was of a previously valid record, and the record is now invalid, it is inserted into
	the observation_transect_error table.
*/

CREATE TRIGGER TRG_UPDATE_TranObsAssoc
ON Observation
AFTER UPDATE
AS
BEGIN
	IF @@ROWCOUNT = 0
		RETURN
	SET NOCOUNT ON;

	IF (UPDATE(location_epsg4326) OR UPDATE(CWS_ecoregion_assignment) OR UPDATE(CWS_transect_assignment))
	BEGIN
		-- Create table variables to hold valid records AND records with errors
		DECLARE @ValidIntersect TABLE
		(
			observation_sid INT,
			transect_id INT,
			validated_buffer INT
		);

		DECLARE @errors TABLE
		(
			observation_sid INT,
			measured_ecoregion_id INT,
			measured_ecoregion_line INT,
			[description] VARCHAR(MAX)
		);

		-- Valid Record:  Ecoregion line from 200m intersect matches what CWS called it.
		INSERT INTO @ValidIntersect (observation_sid, transect_id, validated_buffer)
		SELECT I.observation_sid, T.transect_buffered, 200
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE I.cws_transect_assignment = T.ecoregion_line AND
				I.cws_ecoregion_assignment = T.ecoregion_id;

		-- Valid Record:  Ecoregion line from 5000m intersect matches what CWS called it.
		INSERT INTO @ValidIntersect (observation_sid, transect_id, validated_buffer)
		SELECT I.observation_sid, T.transect_buffered, 5000
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
							WHERE I.Observation_sid = VI.observation_sid) AND
				I.cws_transect_assignment = T.ecoregion_line AND
				I.cws_ecoregion_assignment = T.ecoregion_id;

		-- If the record(s) updated were previously error records and are now valied
		-- remove any existing entry in the observation_transect_error table that may have existed and
		-- insert the now validated record into the observation transect table.
		DELETE FROM observation_transect_error
			WHERE observation_id IN (SELECT observation_sid FROM @ValidIntersect);

		-- Its possible that a valid record will be updated and the update will also be valid
		-- in which case we should perform an upsert
		MERGE dbo.observation_transect WITH (HOLDLOCK) AS dtarget
		USING (SELECT observation_sid, transect_id, validated_buffer
				FROM @ValidIntersect) AS dsource
		ON dtarget.observation_id = dsource.observation_sid
		WHEN MATCHED THEN UPDATE
			SET dtarget.transect_id = dsource.transect_id, dtarget.validated_buffer = dsource.validated_buffer
		WHEN NOT MATCHED THEN
			INSERT (observation_id, transect_id, validated_buffer)
			VALUES (dsource.observation_sid, dsource.transect_id, dsource.validated_buffer);


		-- Invalid Records:
		--	An invalid record could result from an update, where the updated information
		--	is still incorrect OR the updated record was originally a valid record but the
		--  new information is such that it is now invalid.


		-- Intersect does not match CWS assignment or point does not fall within any
		-- transect buffer.
		INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
			SELECT I.observation_sid, T.ecoregion_id, T.ecoregion_line, 'ERROR_differing_line_assignment_200m'
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
							WHERE I.Observation_sid = VI.observation_sid
							UNION
							SELECT Observation_sid FROM @errors e
							WHERE I.Observation_sid = e.observation_sid) AND
				(I.cws_transect_assignment <> T.ecoregion_line OR
				I.cws_ecoregion_assignment <> t.ecoregion_id);

		INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
			SELECT I.observation_sid, T.ecoregion_id, T.ecoregion_line, 'ERROR_differing_line_assignment_5000m'
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
							WHERE I.Observation_sid = VI.observation_sid
							UNION
							SELECT Observation_sid FROM @errors e
							WHERE I.Observation_sid = e.observation_sid) AND
				(I.cws_transect_assignment <> T.ecoregion_line OR
				I.cws_ecoregion_assignment <> t.ecoregion_id);

		INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
			SELECT DISTINCT I.observation_sid, NULL AS ecoregion_id, NULL AS ecoregion_line, 'ERROR_Observation_outside_all_buffered_transects'
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 0
			WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
							WHERE I.Observation_sid = VI.observation_sid
							UNION
							SELECT Observation_sid FROM @errors e
							WHERE I.Observation_sid = e.observation_sid);


		-- If the original record was valid and is now invalid, remove
		-- the record from observation_transect and insert a record into the
		-- observation_transect_error table
		DECLARE @VREC TABLE (observation_sid INT);

		INSERT INTO @VREC(observation_sid)
		SELECT observation_id FROM observation_transect
			WHERE observation_id IN (SELECT observation_sid FROM @errors);

		IF (@@ROWCOUNT > 0)
		BEGIN
			DELETE FROM observation_transect
			WHERE observation_id IN (SELECT observation_sid FROM @errors);

			INSERT INTO observation_transect_error
			SELECT observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description] FROM @errors;

			DELETE FROM @errors
			WHERE observation_sid IN (SELECT observation_sid FROM @VREC);
		END

		-- If the original record was invalid and remains invalid
		-- then simply update the existing record in the observation_transect_error
		-- table.
		UPDATE ote
		   SET ote.error_description = e.[description],
			   ote.measured_ecoregion_id = e.measured_ecoregion_id,
			   ote.measured_ecoregion_line = e.measured_ecoregion_line
		FROM  observation_transect_error ote
		INNER JOIN @errors e
		ON e.observation_sid = ote.observation_id;

	END
END
GO
