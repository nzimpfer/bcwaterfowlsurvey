/*
	This trigger performs a spatial join for each new observation inserted against
	the 200m and 5000m buffered transects.  On success the transect_id and
	validated_buffer columns are updated with appropriate information.  On Failure
	an erorr description is written to the error column.

	Error Conditions:
		* If the join results in a transect_id value of null
		* If the spatial join results in a transect_id value that is different than
		what CWS recorded the observation as. Observation should be evaluated in a
		GIS, and discussion with CWS.
*/

CREATE OR ALTER TRIGGER trg_insert_update_transect
ON Observation
AFTER INSERT, UPDATE
AS
IF (
		UPDATE(location_epsg4326) OR
		UPDATE(cws_transect_assignment) OR
		UPDATE(cws_ecoregion_assignment)
)
BEGIN
	SET NOCOUNT ON;
	
	WITH buff_200m AS (
			-- Intersect any new or updates observations against 200m buffer, using
			-- point-in-polygon method. Returning only those in polygons that fall
			-- in the buffer and CWS assigment matches the buffer it falls in
			SELECT I.observation_sid,
				T.transect_buffered AS transect_id,
				'200 meter' AS validated_buffer,
				NULL AS Error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE I.cws_transect_assignment = T.ecoregion_line AND
					I.cws_ecoregion_assignment = T.ecoregion_id
	)
	, buff_5000m AS (
			-- Intersect any new or updates observations against 5000m buffer, using
			-- point-in-polygon method. Returning only those in polygons that fall
			-- in the buffer and CWS assigment matches the buffer it falls in
			SELECT I.observation_sid,
				T.transect_buffered AS transect_id,
				'5000 meter' AS validated_buffer,
				NULL AS error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE I.cws_transect_assignment = T.ecoregion_line AND
			I.cws_ecoregion_assignment = T.ecoregion_id
	)
	, outside_all_buffers AS (
			-- Return any new updated observations that do not fall outside of 5000m
			-- polygon
			SELECT I.observation_sid,
				NULL as transect_id,
				NULL as validated_buffer,
				'Point outside all buffered transects' as error
			FROM inserted I
			WHERE NOT EXISTS (
				SELECT *
				FROM ref_transect_buffered T
				WHERE I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			)
	)
	, cws_line_differ_200m AS (
			-- Find observations where it falls within the 200m buffer but its a
			-- different buffer than what CWS identified.
			SELECT I.observation_sid,
			T.transect_buffered as transect_id,
			'200 meter' as validated_buffer,
			CASE WHEN I.cws_transect_assignment <> T.ecoregion_line THEN 'Incorrect transect assigment'
				WHEN I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect ecoregion assigment'
				WHEN I.cws_transect_assignment <> T.ecoregion_line AND
				I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect transect & ecoregion assigment'
				END AS error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE I.cws_transect_assignment <> T.ecoregion_line OR
				I.cws_ecoregion_assignment <> T.ecoregion_id
	)
	, cws_line_differ_5000m AS (
			-- Find observations where it falls within the 5000m buffer but its a
			-- different buffer than what CWS identified.
			SELECT I.observation_sid,
			T.transect_buffered as transect_id,
			'5000 meter' as validated_buffer,
			CASE WHEN I.cws_transect_assignment <> T.ecoregion_line THEN 'Incorrect transect assigment'
				WHEN I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect ecoregion assigment'
				WHEN I.cws_transect_assignment <> T.ecoregion_line AND
				I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect transect & ecoregion assigment'
				END AS error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE I.cws_transect_assignment <> T.ecoregion_line OR
				 I.cws_ecoregion_assignment <> T.ecoregion_id
	)
	, intersect_combine AS (
		SELECT * from buff_200m

		UNION

		-- This exists clause is present becuase if an observation fall in a 200m
		-- buffer it will also fall in the 5000m buffer
		SELECT *
		FROM buff_5000m
		WHERE NOT EXISTS (
			SELECT 1 from buff_200m
			WHERE buff_5000m.observation_sid = buff_200m.observation_sid
		)

		UNION

		SELECT * FROM outside_all_buffers

		UNION

		SELECT * FROM cws_line_differ_200m

		UNION

		SELECT *
		FROM cws_line_differ_5000m
		WHERE NOT EXISTS (
			SELECT 1 FROM cws_line_differ_200m
			WHERE cws_line_differ_5000m.observation_sid = cws_line_differ_200m.observation_sid
			)
	)
	UPDATE obs
	SET obs.transect_id = d.transect_id,
		obs.validated_buffer = d.validated_buffer,
		obs.error = d.error
	FROM observation obs
	INNER JOIN intersect_combine d
	ON obs.observation_sid = d.observation_sid;
END
GO
