/*
	This trigger performs a spatial join for each new observation inserted against 
	the 200m buffered transects.  The resulting observation_id and transect_fid are 
	inserted into the observation_transect table.  
	
	Error Conditions:
		If the join results in a transect_id value of null, then a[the] record is inserted 
		into the observation_transect_error table, indicating that the observation does not 
		fall within the survey transect boundary (width).  These observations should be sent 
		back to CWS for correction.

		If the spatial join results in a transect_id value that is different than what CWS recorded 
		the observation as, the record is inserted into the observation_transect_error table.
		Observation should be evaluated in a GIS, and discussion with CWS.
*/

CREATE TRIGGER TRG_INSERT_TranObsAssoc
ON Observation
AFTER INSERT
AS
BEGIN
	IF @@ROWCOUNT = 0 
		RETURN
	SET NOCOUNT ON 

	-- Create table variables to hold valid records AND records with errors
	DECLARE @ValidIntersect TABLE
	(
		observation_sid INT,
		transect_id INT,
		validated_buffer INT
	);

	DECLARE @errors TABLE
	(
		observation_sid INT,
		measured_ecoregion_id INT,
		measured_ecoregion_line INT,
		[description] VARCHAR(MAX) 
	);

	-- Valid Record:  Ecoregion line from 200m intersect matches what CWS called it.
	INSERT INTO @ValidIntersect (observation_sid, transect_id, validated_buffer)
		SELECT I.observation_sid, T.transect_buffered, 200
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1 
			WHERE I.cws_transect_assignment = T.ecoregion_line AND 
				I.cws_ecoregion_assignment = T.ecoregion_id;

	-- Valid Record:  Ecoregion line from 5000m intersect matches what CWS called it.
	INSERT INTO @ValidIntersect (observation_sid, transect_id, validated_buffer)
	SELECT I.observation_sid, T.transect_buffered, 5000
		FROM inserted I
		JOIN ref_transect_buffered T
		ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1 
		WHERE NOT EXISTS (SELECT Observation_sid
						  FROM @ValidIntersect VI
						  WHERE I.Observation_sid = VI.observation_sid)
		AND I.cws_transect_assignment = T.ecoregion_line AND
			I.cws_ecoregion_assignment = T.ecoregion_id;

	-- Insert all of the valid records to observation_transect.
	INSERT INTO observation_transect
		SELECT observation_sid, transect_id, validated_buffer 	
		FROM @ValidIntersect

	-- Intersect does not match CWS transect assignment
	-- Prevent looking for errors in records where an error has been found already
	-- or intersect matches cws provided information.
	INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
		SELECT I.observation_sid, T.ecoregion_id, T.ecoregion_line, 'ERROR_differing_line_assignment_200m'
		FROM inserted I
		JOIN ref_transect_buffered T
		ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
		WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
						WHERE I.Observation_sid = VI.observation_sid
						UNION
						SELECT Observation_sid FROM @errors e
						WHERE I.Observation_sid = e.observation_sid)
		AND (I.cws_transect_assignment <> T.ecoregion_line OR 
			I.cws_ecoregion_assignment <> T.ecoregion_id);


	INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
		SELECT I.observation_sid, T.ecoregion_id, T.ecoregion_line, 'ERROR_differing_line_assignment_5000m'
		FROM inserted I
		JOIN ref_transect_buffered T
		ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1 
		WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
						WHERE I.Observation_sid = VI.observation_sid
						UNION
						SELECT Observation_sid FROM @errors e
						WHERE I.Observation_sid = e.observation_sid) 
		AND (I.cws_transect_assignment <> T.ecoregion_line OR
			 I.cws_ecoregion_assignment <> T.ecoregion_id);

	--	Point does not fall within any  transect buffer.
	INSERT INTO @errors (observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description])
		SELECT DISTINCT I.observation_sid, NULL AS ecoregion_id, NULL AS ecoregion_line, 'ERROR_Observation_outside_all_buffered_transects'
		FROM inserted I
		JOIN ref_transect_buffered T
		ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 0
		WHERE NOT EXISTS (SELECT Observation_sid FROM @ValidIntersect VI
						WHERE I.Observation_sid = VI.observation_sid
						UNION
						SELECT Observation_sid FROM @errors e
						WHERE I.Observation_sid = e.observation_sid);


	-- Insert all of the valid records to observation_transect.
	INSERT INTO observation_transect_error (observation_id, measured_ecoregion_id, measured_ecoregion_line, error_description)
		SELECT observation_sid, measured_ecoregion_id, measured_ecoregion_line, [description]
		FROM @errors

END
GO
