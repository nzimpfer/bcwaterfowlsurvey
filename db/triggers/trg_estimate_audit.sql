/*
	Replicate SQL Server CDC functionality but on a permenant basis.
*/
CREATE TRIGGER TRG_estimate_audit
ON estimate
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @operation INT

	-- insert only
	IF NOT EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @operation = 2;
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by], 
		[operation], [species_id], [year], [ecoregion_id], [estimate], [variance])
		SELECT [estimate_sid], 
			CURRENT_TIMESTAMP, 
			SYSTEM_USER, 
			@operation, 
			[species_id], 
			[year], 
			[ecoregion_id], 
			[estimate], 
			[variance]
		FROM inserted
		END

		-- delete only
		IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)
		BEGIN
		SET @operation = 1;
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by], 
		[operation], [species_id], [year], [ecoregion_id], [estimate], [variance])
		SELECT [estimate_sid], 
			CURRENT_TIMESTAMP, 
			SYSTEM_USER, 
			@operation, 
			[species_id], 
			[year], 
			[ecoregion_id], 
			[estimate], 
			[variance]
		FROM deleted
	END

	-- update
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @operation = 3;
		INSERT INTO estimate_audit ([estimate_id],[operation_on], [operation_by], 
		[operation], [species_id], [year], [ecoregion_id], [estimate], [variance])
		SELECT [estimate_sid], 
			CURRENT_TIMESTAMP, 
			SYSTEM_USER, 
			@operation, 
			[species_id], 
			[year], 
			[ecoregion_id], 
			[estimate], 
			[variance]
		FROM deleted

		SET @operation = 4;
		INSERT INTO estimate_audit ([estimate_id], [operation_on], [operation_by], 
		[operation], [species_id], [year], [ecoregion_id], [estimate], [variance])
		SELECT [estimate_sid], 
			CURRENT_TIMESTAMP, 
			SYSTEM_USER, 
			@operation, 
			[species_id], 
			[year], 
			[ecoregion_id], 
			[estimate], 
			[variance]
		FROM deleted
	END
END
GO