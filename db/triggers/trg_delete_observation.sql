CREATE OR ALTER TRIGGER trg_delete_observation
ON observation
AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @action_type CHAR(1) = 'D';

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO audit_observation (
			[observation_id],
			[updated_on],
			[action_type],
			[cws_record_identifier],
			[cws_point_identifier],
			[observation_datetime_utc],
			[location_epsg4326],
			[cws_ecoregion_assignment],
			[cws_transect_assignment],
			[species_id],
			[social_group_id],
			[males],
			[females],
			[unknown]
		)
		SELECT [observation_sid],
			CURRENT_TIMESTAMP,
			@action_type,
			[cws_record_identifier],
			[cws_point_identifier],
			[observation_datetime_utc],
			[location_epsg4326],
			[cws_ecoregion_assignment],
			[cws_transect_assignment],
			[species_id],
			[social_group_id],
			[males],
			[females],
			[unknown]
		FROM deleted
	END
END
GO
