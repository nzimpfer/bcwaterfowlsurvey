/*
	checks whether columns 4- 12 (cws_record_identifier through unknown) of
	an observation record have changed.  If so, create and audit record, capturing
	the original values of the record.

	-- https://www.sqlservercentral.com/articles/columns_updated-and-triggers
	SELECT TABLE_NAME
		, COLUMN_NAME
		,((COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID') - 1) / 8) + 1  AS ByteNum
		,POWER(2,(COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID') - 1) % 8)  AS bitVal
	FROM INFORMATION_SCHEMA.COLUMNS C
	WHERE C.TABLE_NAME = 'observation' 
	and C.COLUMN_NAME in ('observation_datetime_utc','location_epsg4326','cws_ecoregion_assignment','cws_transect_assignment','species_id','social_group_id',
		'males','females','unknown')
	ORDER BY 3,4
	GO

	SELECT X.ByteNum
	, SUM(X.bitVal)  AS bitval
	FROM (
		SELECT TABLE_NAME
			, COLUMN_NAME
			,((COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID') - 1) / 8) + 1  AS ByteNum
			,POWER(2,(COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID') - 1) % 8)  AS bitVal
		FROM INFORMATION_SCHEMA.COLUMNS C
		where C.TABLE_NAME = 'observation' and
		C.COLUMN_NAME in ('observation_datetime_utc','location_epsg4326','cws_ecoregion_assignment','cws_transect_assignment','species_id','social_group_id',
			'males','females','unknown')
	) AS X
	GROUP BY X.ByteNum
	ORDER BY X.ByteNum
*/
CREATE OR ALTER TRIGGER trg_observation_modified
ON observation
AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @action_type CHAR(1) = 'U';

  IF ( (SUBSTRING(COLUMNS_UPDATED(), 1, 1) & 248 > 0)
		OR (SUBSTRING(COLUMNS_UPDATED(), 2, 1) & 15 > 0) )
  
	INSERT INTO audit_observation (
		[observation_id],
		[updated_on],
		[action_type],
		[cws_record_identifier],
		[cws_point_identifier],
		[observation_datetime_utc],
		[location_epsg4326],
		[cws_ecoregion_assignment],
		[cws_transect_assignment],
		[species_id],
		[social_group_id],
		[males],
		[females],
		[unknown]
	)
	SELECT [observation_sid],
		CURRENT_TIMESTAMP,
		@action_type,
		[cws_record_identifier],
		[cws_point_identifier],
		[observation_datetime_utc],
		[location_epsg4326],
		[cws_ecoregion_assignment],
		[cws_transect_assignment],
		[species_id],
		[social_group_id],
		[males],
		[females],
		[unknown]
	FROM deleted	
END
GO
