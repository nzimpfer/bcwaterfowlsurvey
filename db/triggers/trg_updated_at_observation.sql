CREATE TRIGGER trg_updated_at_observation ON observation
AFTER UPDATE
AS
BEGIN
   SET NOCOUNT ON;

   UPDATE dbo.observation
   SET updated_at = CURRENT_TIMESTAMP
   WHERE EXISTS (
     SELECT 1
     FROM inserted i
     WHERE i.observation_sid = observation.observation_sid
   )
END
GO
