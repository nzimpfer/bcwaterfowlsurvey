/*
	This trigger writes an audit record to the audit_observation table whenever
	a record is updated or deleted.  

	NOTE: The audit record captures the ORIGINAL values of the record.
*/

CREATE TRIGGER TRG_UPDATE_DEL_Observation 
ON observation
AFTER UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @action_type CHAR(1)

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)
	BEGIN
    SET @action_type = 'D';

	INSERT INTO audit_observation ([observation_id], [updated_on], [action_type], [cws_record_identifier]
		, [cws_point_identifier], [observation_datetime_utc], [location_epsg4326], [cws_ecoregion_assignment]
		, [cws_transect_assignment], [species_id], [social_group_id], [males], [females], [unknown])
	SELECT [observation_sid], CURRENT_TIMESTAMP, @action_type, [cws_record_identifier], [cws_point_identifier], 
		[observation_datetime_utc], [location_epsg4326], [cws_ecoregion_assignment], [cws_transect_assignment]
		, [species_id], [social_group_id], [males], [females], [unknown]
	FROM deleted
	END

	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
    SET @action_type = 'U';

	INSERT INTO audit_observation ([observation_id], [updated_on], [action_type], [cws_record_identifier]
		, [cws_point_identifier], [observation_datetime_utc], [location_epsg4326], [cws_ecoregion_assignment]
		, [cws_transect_assignment], [species_id], [social_group_id], [males], [females], [unknown])
	SELECT [observation_sid], CURRENT_TIMESTAMP, @action_type, [cws_record_identifier], [cws_point_identifier], 
		[observation_datetime_utc], [location_epsg4326], [cws_ecoregion_assignment], [cws_transect_assignment]
		, [species_id], [social_group_id], [males], [females], [unknown]
	FROM deleted
	END
END
GO
