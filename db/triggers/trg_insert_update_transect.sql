/*
	This trigger performs a spatial join for each new observation inserted against
	the 200m and 5000m buffered transects.  On success the transect_id and
	validated_buffer columns are updated with appropriate information.  On Failure
	an erorr description is written to the error column.

	Error Conditions:
		* If the join results in a transect_id value of null
		* If the spatial join results in a transect_id value that is different than
		what CWS recorded the observation as. Observation should be evaluated in a
		GIS, and discussion with CWS.
*/

CREATE OR ALTER TRIGGER trg_insert_update_transect
ON Observation
AFTER INSERT, UPDATE
AS
BEGIN
	IF @@ROWCOUNT = 0
		RETURN
	SET NOCOUNT ON;

	-- Update the observation record with the transect_id and the validated_buffer
	-- Valid Record:  Ecoregion line from 200m intersect matches what CWS called it.
	WITH buff_200m AS (
			SELECT I.observation_sid,
				T.transect_buffered,
				'200 meter' AS buffer
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE I.cws_transect_assignment = T.ecoregion_line AND
					I.cws_ecoregion_assignment = T.ecoregion_id
	)
	, buff_5000m AS (
			SELECT I.observation_sid,
				T.transect_buffered,
				'5000 meter' AS buffer
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE NOT EXISTS (
				SELECT 1
				FROM buff_200m VI
				WHERE I.Observation_sid = VI.observation_sid
			) AND
			I.cws_transect_assignment = T.ecoregion_line AND
			I.cws_ecoregion_assignment = T.ecoregion_id
	)
	, intersect_combine AS (
		SELECT * from buff_200m
		UNION ALL
		SELECT * FROM buff_5000m
	)
	UPDATE obs
		SET obs.transect_id = val.transect_buffered,
			obs.validated_buffer = val.validated_buffer,
			obs.error = NULL
		FROM observation obs
		INNER JOIN (
			SELECT observation_sid,
				transect_buffered,
				MIN(buffer) AS validated_buffer
			FROM intersect_combine
			GROUP BY observation_sid, transect_buffered
		) val
		ON obs.Observation_sid = val.observation_sid;


	-- There are 2 error cases.  1.) the transect doesn't validate against
	-- any buffer or the transect validated against one or more buffers but
	-- it doesn't match the transect cws called it.
	WITH outside_all_buffers AS (
			SELECT I.observation_sid,
				NULL as transect_id,
				NULL as validated_buffer,
				'Point outside all buffered transects' as error
			FROM inserted I
			WHERE NOT EXISTS (
			SELECT *
			FROM ref_transect_buffered T
			WHERE I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			)
	)
	, cws_line_differ_200m AS (
			SELECT I.observation_sid,
			T.transect_buffered as transect_id,
			'200 meter' as validated_buffer,
			CASE WHEN I.cws_transect_assignment <> T.ecoregion_line THEN 'Incorrect transect assigment'
				WHEN I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect ecoregion assigment'
				WHEN I.cws_transect_assignment <> T.ecoregion_line AND
				I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect transect & ecoregion assigment'
				END AS error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_200m_epsg4326) = 1
			WHERE I.cws_transect_assignment <> T.ecoregion_line OR
				I.cws_ecoregion_assignment <> T.ecoregion_id
	)
	, cws_line_differ_5000m AS (
			SELECT I.observation_sid,
			T.transect_buffered as transect_id,
			'5000 meter' as validated_buffer,
			CASE WHEN I.cws_transect_assignment <> T.ecoregion_line THEN 'Incorrect transect assigment'
				WHEN I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect ecoregion assigment'
				WHEN I.cws_transect_assignment <> T.ecoregion_line AND
				I.cws_ecoregion_assignment <> T.ecoregion_id THEN 'Incorrect transect & ecoregion assigment'
				END AS error
			FROM inserted I
			JOIN ref_transect_buffered T
			ON I.location_epsg4326.STIntersects(T.geog_5000m_epsg4326) = 1
			WHERE I.cws_transect_assignment <> T.ecoregion_line OR
				 I.cws_ecoregion_assignment <> T.ecoregion_id
	)
	, all_errors AS (
		SELECT * FROM outside_all_buffers

		UNION

		SELECT * FROM cws_line_differ_200m

		UNION

		SELECT *
		FROM cws_line_differ_5000m
		WHERE NOT EXISTS (
			SELECT 1 FROM cws_line_differ_200m
			WHERE cws_line_differ_5000m.observation_sid = cws_line_differ_200m.observation_sid
			)
	)
	UPDATE obs
	SET obs.error = e.error,
		obs.transect_id = e.transect_id,
		obs.validated_buffer = e.validated_buffer
	FROM observation obs
	INNER JOIN all_errors e
	ON obs.observation_sid = e.observation_sid;
END
GO
