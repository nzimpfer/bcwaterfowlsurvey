CREATE TRIGGER trg_updated_at_transect_buffered ON ref_transect_buffered
AFTER UPDATE
AS
BEGIN
   SET NOCOUNT ON;

   UPDATE dbo.ref_transect_buffered
   SET updated_at = CURRENT_TIMESTAMP
   WHERE EXISTS (
     SELECT 1
     FROM inserted i
     WHERE i.transect_buffered = ref_transect_buffered.transect_buffered
   )
END
GO
