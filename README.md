# README #

* This repository holds the table definitions and documentation for creation of the database to hold data and routines for the British Columbia 
  Waterfowl survey.

* All of the documentation can be found in the wiki.

### Team ###

* Admin: Nathan Zimpfer 
* Team: Guthrie Zimmerman
