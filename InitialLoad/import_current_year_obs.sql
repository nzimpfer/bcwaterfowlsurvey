USE [ifw9mb_BCWaterfowlSurvey]
GO

-- create temporary table
CREATE TABLE ##BC_raw_import (
	[observation_id]			INTEGER IDENTITY(1,1),
	cws_record_id				INTEGER,
	cws_point_id				VARCHAR(10),
	longitude					DECIMAL(15,12),
	latitude					DECIMAL(15,13),
	species						CHAR(4),
	observation_datetime_utc	VARCHAR(100),
	cws_ecoregion_assignment	VARCHAR(100),
	cws_transect_assignment		INTEGER,
	social_group_id				INTEGER,
	males						INTEGER,
	females						INTEGER,
	unknown						INTEGER,
	obs_timestamp				DATETIME,  -- begin columns computed
	location					GEOGRAPHY
)
GO

select top 100  * from ##BC_raw_import

-- insert raw data
-- < currently done via bcp.. placeholder once I have been assigned bulk insert privledges >

-- create geography point column from coordinates
UPDATE ##BC_raw_import
SET [location] = geography::STGeomFromText('POINT (' + 
	CAST([longitude] AS VARCHAR(20)) + ' ' + CAST([latitude] AS VARCHAR(20)) + ')', 4326)
GO

-- create timestamp column from datepart columns
UPDATE ##BC_raw_import 
SET obs_timestamp = CAST(observation_datetime_utc AS DATETIME) 
GO

SELECT observation_id
	, obs_timestamp
	, location
	, species
	, social_group_id
	, males
	, females
	, unknown
	, cws_record_id				
	, cws_point_id					
	, e.ecoregion
	, cws_transect_assignment		
FROM ##BC_raw_import		
JOIN ref_ecoregion e
	ON cws_ecoregion_assignment = e.[description]
GO

--- insert into obseveration table
INSERT INTO Observation (observation_datetime_utc, location_epsg4326, species_id,
	social_group_id, males, females, unknown, cws_record_identifier, cws_point_identifier, cws_ecoregion_assignment,
	cws_transect_assignment)
SELECT obs_timestamp
	, location
	, species
	, social_group_id
	, males
	, females
	, unknown
	, cws_record_id				
	, cws_point_id					
	, e.ecoregion	
	, cws_transect_assignment		
FROM ##BC_raw_import tmp		
JOIN ref_ecoregion e
	on tmp.cws_ecoregion_assignment = e.description
GO

SELECT * FROM observation_transect_error

DROP TABLE ##BC_raw_import;
