/*
	Insert missed segement based on know ecoregion, transect, and segment 
	numbering.  Translate that to an id value before inserting into transect_nowflown.
*/
CREATE TABLE #not_flown_transects (
	  YEAR INT
	, ecoregion INT
	, transect INT
	, segment INT
)


INSERT INTO #not_flown_transects ([year],[ecoregion],[transect],[segment])
VALUES
(2006,1,1,1), (2006,1,2,1), (2006,1,3,1), (2006,1,3,2), 
(2006,1,4,1), (2006,1,4,2), (2006,1,4,3), (2006,1,5,1), 
(2006,1,6,1), (2006,2,1,1), (2006,3,1,1), (2006,3,2,1),
(2006,3,3,1), (2006,9,10,1), (2006,10,9,1), (2006,10,9,2), 
(2006,10,10,1), (2007,1,1,1), (2007,1,2,1), (2007,1,4,1),
(2007,1,4,1), (2007,1,4,3), (2007,1,6,1), (2007,1,7,1);
GO


SELECT A.*, B.transect
FROM #not_flown_transects a
INNER JOIN ref_transect b
	ON a.ecoregion = b.ecoregion_id
	AND a.transect = b.transect
	AND a.segment = b.segment


INSERT INTO transect_notflown ([year], [transect_id])
SELECT a.[year], b.[transect] 
FROM #not_flown_transects a
INNER JOIN ref_transect b
	ON a.ecoregion = b.ecoregion_id
	AND a.transect = b.transect
	AND a.segment = b.segment